/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  27/07/17 14:49:58
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>


#define NX 4
#define NY 2
#define NZ 2

#define ROW_ALIGNMENT 4

typedef float real_t;

typedef real_t row_t[NX] __attribute__((aligned(ROW_ALIGNMENT)));
typedef row_t plane_t[NY];
typedef plane_t (*restrict volume_t);

void print_row( row_t *restrict row)
{
	int i=0;
	real_t *p = (real_t*) row;

	for(i=0; i < NX; i++)
		fprintf(stderr, "%.1f  ", *p++);

	fprintf(stderr, "\n");
};

void print_plane( plane_t *restrict  plane)
{
	int i=0, j=0;
	real_t *p = (real_t*) plane;
	const int row_size = NX;


	fprintf(stderr, "Imprimiendo plano!\n");
	for(j=0; j < NY; j++) {
		print_row( (row_t*) p );
		p += row_size;
	}

	fprintf(stderr, "\n");
};
/* define a row on top of real_t type */
// typedef real_t (*restrict volume_t)[NY][NX];

volume_t alloc_volume(void)
{
	void *p = malloc( NZ * NY * NX * sizeof(real_t));

	if ( p ) return ( (volume_t) p);
	else { fprintf(stderr, "Cant allocate volume\n"); abort(); }
}

void free_volume(volume_t v)
{
	if ( v ) {free(v); v=NULL;}
};

void init_volume(volume_t v)
{
	int x,y,z;
	real_t value = 0.0;

	for(z=0; z<NZ; z++){
		for(y=0; y<NY; y++) {
			for(x=0; x<NX; x++){
				v[z][y][x] = value++;
			}
		}
	}
}

void print_volume(volume_t v)
{
	int x,y,z;

	for(z=0; z<NZ; z++){
		for(y=0; y<NY; y++) {
			for(x=0; x<NX; x++){
				fprintf(stderr, "%.1f  ", v[z][y][x] );
			}
			fprintf(stderr, "\n");
		}
		fprintf(stderr, "\n\n\n");
	}
}

void print_volume_seq(volume_t v)
{
	int n = NZ * NY * NX;
	real_t* p = (real_t*) v;

	for( int i=0; i < n; i++)
		fprintf(stderr, "[%d] - %.1f\n", i, *p++ );

}
void set_row_to_constant( row_t *restrict row, const real_t value)
{
	for(int i=0; i < NX, i++)
};
int main(int argc, const char *argv[])
{
	fprintf(stderr, "Inicio del programa\n");
	
	volume_t v = alloc_volume();

	init_volume(v);
	print_volume(v);
	print_volume_seq(v);

	print_row( (row_t*) v[0][1] );
	print_plane( (plane_t*) v[0]);

	free_volume(v);

	fprintf(stderr, "Fin del programa\n");
	return 0;
}
