#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef float real_t;

typedef struct
{
	real_t real;
	real_t imag;
} complex_t;


void print_matrix(complex_t* arr, int nrows, int ncols, const char* message)
{
	if ( message ) fprintf(stderr, "%s\n", message);

	for(int row=0; row < nrows; row++){
		for(int col=0; col < ncols; col++){
			int idx = row * ncols + col;
			fprintf(stderr, "%.1f %.1f ", arr[idx].real, arr[idx].imag);
		}

		fprintf(stderr, "\n");
	}
}


void init_matrix(complex_t* arr, int nrows, int ncols)
{
	for(int row=0; row < nrows; row++){
		for(int col=0; col < ncols; col++){
			int idx = row * ncols + col;
			arr[idx].real = idx;
			arr[idx].imag = idx;
		}
	}

}


void set_column( complex_t* arr, int nrows, int ncols, real_t val)
{
	real_t *tmp = (real_t*) arr;

	for(int row=0; row < nrows; row++){
		
		*tmp = val;

		/* move to the next column */
		tmp = tmp + ncols * sizeof(complex_t) / sizeof(real_t);

	}
}

int main(int argc, char *argv[])
{
	int ncols = 5;
	int nrows = 3;

	complex_t* matrix = (complex_t*) malloc( ncols * nrows * sizeof(complex_t));
	memset( matrix, 0, ncols * nrows * sizeof(complex_t));

	init_matrix( matrix, nrows, ncols);

	print_matrix( matrix, nrows, ncols, "Initial matrix");

	set_column( matrix, nrows, ncols, -3.0);

	print_matrix( matrix, nrows, ncols, "Final matrix");

	free(matrix);
    return 0;
}
