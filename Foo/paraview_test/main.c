/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  How to export a field to ParaView format
 *
 *        Version:  1.0
 *        Created:  04/08/2017 07:20:19
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Samuel Rodriguez Bernabeu (), samuel.rodriguez@bsc.es
 *   Organization:  Barcelona Supercomputing Center
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

/* number of cells on each direction */
#define NX 3
#define NY 4
#define NZ 2

/* discretization deltas */
#define DX 0.1
#define DY 0.2
#define DZ 0.05

/* data type */
typedef float real_t;

void export_to_ParaView(real_t field[NZ][NY][NX])
{
	FILE *fout = fopen("field.vtk", "w");

	/* write paraview header */
	fprintf(fout, "#vtk DataFile Version 2.0\n");

	/* write a brief data description */
	fprintf(fout, "Velocity field\n");

	/* write data format */
	fprintf(fout, "ASCII\n");

	/* write dataset structure */
	fprintf(fout, "DATASET STRUCTURED_GRID\n");

	/* write dataset attributes */
	fprintf(fout, "DIMENSIONS %d %d %d\n", (int) NX, (int) NY, (int) NZ );
	fprintf(fout, "ORIGIN %d %d %d\n", 0, 0, 0);
	fprintf(fout, "SPACING %f %f %f\n", (real_t) DX, (real_t) DY, (real_t) DZ);

	for(int z=0; z < NZ; z++){
		for(int y=0; y < NY; y++){
			for(int x=0; x < NX; x++){
				fprintf(stderr, "%.2f  ", field[z][y][x]);
			}
		}
	}

	fprintf(stderr, "\n");

	fclose(fout);
};

int main(int argc, const char *argv[])
{
	fprintf(stderr, "Creating an small 3D scalar field\n");

	real_t field[NZ][NY][NX];
	real_t value = 0.0;

	for(int z=0; z < NZ; z++){
		for(int y=0; y < NY; y++){
			for(int x=0; x < NX; x++){
				field[z][y][x] = value++;
			}
		}
	}
	
	export_to_ParaView(field);

	fprintf(stderr, "End of the program\n");
	return 0;
}
