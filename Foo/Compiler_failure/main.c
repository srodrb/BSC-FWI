/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  WTF??
 *
 *        Version:  1.0
 *        Created:  04/08/2017 16:45:40
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Samuel Rodriguez Bernabeu (), samuel.rodriguez@bsc.es
 *   Organization:  Barcelona Supercomputing Center
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>

typedef enum{c00, c11} var_t;

float foo(const var_t v) { return 7.0; };

int main(int argc, const char *argv[])
{
	const float c11 = foo(c11);

	return 0;
}

