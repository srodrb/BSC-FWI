/*
 * =====================================================================================
 *
 *       Filename:  fwi_kernel.h
 *
 *    Description:  Kernel propagator implementation
 *
 *        Version:  1.0
 *        Created:  14/12/15 12:10:24
 *       Revision:  none
 *       Compiler:  icc
 *
 *         Author:  Samuel Rodriguez Bernabeu, samuel.rodriguez@bsc.es
 *   Organization:  Barcelona Supercomputing Center
 *
 * =====================================================================================
 */

#ifndef _FWI_KERNEL_H_
#define _FWI_KERNEL_H_

#include "fwi_propagator.h"


void set_field_to_random (volume_t volume, field_t idx);
void set_field_to_constant(volume_t volume, field_t idx, const real_t value);

void write_field( volume_t volume, field_t idx, FILE *stream);
void read_field( volume_t volume, field_t idx, FILE *stream);
void print_field( volume_t volume, field_t idx, const char* message );

volume_t alloc_volume (void);
void free_volume ( volume_t volume );

void export_to_ParaView(volume_t volume,
		const real_t dy,
		const real_t dx,
		const real_t dz,
		const integer timestep);

/* --------------- I/O RELATED FUNCTIONS -------------------------------------- */

void load_initial_model ( const real_t waveletFreq, volume_t volume );

void write_snapshot (char         *folder,
                    const int     suffix,
                    volume_t      volume );

void read_snapshot (char          *folder,
                    const int     suffix,
                    volume_t       volume );

/* --------------- WAVE PROPAGATOR FUNCTIONS --------------------------------- */

void propagate (time_d        direction,
                volume_t 	  volume,
                real_t        *wvl,
                int           timesteps,
                int           ntbwd,
                real_t        dt,
                real_t        dy,
                real_t        dx,
                real_t        dz,
                integer       stacki,
                char          *folder,
                real_t        *dataflush );

#endif /* end of _FWI_KERNEL_H_ definition */
