/*
 * =====================================================================================
 *
 *       Filename:  fwi_main.c
 *       
 *
 *    Description:  Main routine for FWI mock up
 *
 *        Version:  1.0
 *        Created:  27/07/17 09:39:13
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Samuel Rodriguez Bernabeu, samuel.rodriguez@bsc.es
 *   Organization:  Barcelona Supercomputing Center
 *
 * =====================================================================================
 */
#include "fwi_kernel.h"

void kernel( propagator_t propagator, real_t waveletFreq, int shotid, char* outputfolder)
 {
    /*
     * --------------------------------------------------------
     *                     Local variables
     * --------------------------------------------------------
     */
    double start_t, end_t; // time counters
    int stacki;
    integer dimmz, dimmx, dimmy;
    integer numberOfCells = NZF * NXF * NYF;
    int forw_steps, back_steps;
    real_t vmin;
    real_t dt;
    real_t dz, dx, dy;
    real_t *wavelet;
    real_t nt, t0, vm, arg;
    char shotfolder[200];
    char fnameGradient[300];
    char fnamePrecond[300];
    FILE *fgradient;
    FILE *fprecond;
    volume_t volume;

    /* build output folder name */
    sprintf( shotfolder, "%s/shot.%05d", outputfolder, shotid);
    load_shot_parameters( shotid, &vmin, &stacki, &dt, &forw_steps, &back_steps, &dz, &dx, &dy, &dimmz, &dimmx, &dimmy, outputfolder);

	/* allocate shot memory */
    volume = alloc_volume ();

	/* load initial model from a binary file */
    load_initial_model ( waveletFreq, volume);
    write_snapshot(shotfolder, 0, volume);

    /*
     * --------------------------------------------------------
     *    Create input the wavelet for the current frequency
     * --------------------------------------------------------
     */

    /* allocate wavelet memory */
    nt  = (forw_steps > back_steps) ? forw_steps: back_steps;
    wavelet = (real_t*) __malloc( ALIGN_REAL, nt * sizeof(real_t) );

    t0 = 1.0 / waveletFreq;
    vm = waveletFreq * waveletFreq;

    for(int i = 0; i < nt; i++) {
        arg = M_PI * (i * dt - t0);
        arg *= arg;
        wavelet[i] = (1.0 - 2.0 * vm * arg) * expf(-vm * arg);
    }


    /*
     * --------------------------------------------------------
     *       Run wave propagator kernel (heavy computation)
     * --------------------------------------------------------
     */
    switch( propagator )
    {
    case( RTM_KERNEL ):
    {
        start_t = dtime();

        propagate ( FORWARD,
					volume, wavelet,
					forw_steps, back_steps -1,
					dt,
					1.0/dy,1.0/dx,1.0/dz,
					stacki,
					shotfolder,
					NULL /* io_buffer */	);

        end_t = dtime();
        print_stats("Forward propagation finished in %lf seconds", end_t - start_t );


        start_t = dtime();
        propagate ( BACKWARD,
					volume, wavelet,
					forw_steps, back_steps -1,
					dt,
					1.0/dy,1.0/dx,1.0/dz,
					stacki,
					shotfolder,
					NULL /* io_buffer */	);

        end_t = dtime();
        print_stats("Backward propagation finished in %lf seconds", end_t - start_t );

#if 0
#ifdef DO_NOT_PERFORM_IO
        print_info("Warning: we are not creating gradient nor preconditioner "
         "fields, because IO is not enabled for this execution" );
#else
        sprintf( fnameGradient, "%s/gradient_%05d.dat", shotfolder, shotid );
        sprintf( fnamePrecond , "%s/precond_%05d.dat" , shotfolder, shotid );

        fgradient = safe_fopen( fnameGradient, "wb", __FILE__, __LINE__ );
        fprecond  = safe_fopen( fnamePrecond , "wb", __FILE__, __LINE__ );

        print_info("Storing local preconditioner field in %s", fnameGradient );
        safe_fwrite( io_buffer, sizeof(real_t), numberOfCells * 12, fgradient, __FILE__, __LINE__ );

        print_info("Storing local gradient field in %s", fnamePrecond);
        safe_fwrite( io_buffer, sizeof(real_t), numberOfCells * 12, fprecond , __FILE__, __LINE__ );

        safe_fclose( fnameGradient, fgradient, __FILE__, __LINE__ );
        safe_fclose( fnamePrecond , fprecond , __FILE__, __LINE__ );
#endif
#endif /* Incremental compilation */
        break;
    }
    case( FM_KERNEL  ):
    {
        start_t = dtime();
        propagate ( FWMODEL,
					volume, wavelet,
					forw_steps, back_steps -1,
					dt,
					1.0/dy,1.0/dx,1.0/dz,
					stacki,
					shotfolder,
					NULL /* io_buffer */	);


        end_t = dtime();
        print_stats("Forward Modelling finished in %lf seconds", end_t - start_t );
        break;
    }
    default:
    {
        print_error("Invalid propagation identifier\n");
        abort();
    }
    }

    /* free local memory allocated by the worker */
    free_volume( volume );
    __free( wavelet );
};

int main(int argc, const char *argv[])
{
	/*
     * --------------------------------------------------------
     *                     Local variables
     * --------------------------------------------------------
     */
    double tstart, tend; // timers
    real_t lenz,lenx,leny;
    real_t vmin;
    real_t srclen; // unused (legacy code)
    real_t rcvlen; // unused (legacy code)
    real_t dx, dy, dz; // discretization deltas
    real_t waveletFreq;
    real_t dts, cfl, dt; 
    integer dimmz, dimmx, dimmy;
    int stacki;
	const integer numberOfCells = NZF * NXF * NYF;
    int nshots = 1;
    int gradient_iterations = 1;
    int test_iterations = 1;
    int nfreqs;
    int forw_steps, back_steps;
    real_t *frequencies;
    char outputfolder[200];
    char shotfolder[200];

    /* Load simulation parameters */
    read_simulation_parameters( argv[1], &lenz, &lenx, &leny, &vmin, &srclen, &rcvlen, outputfolder);
    read_freqlist( argv[2], &nfreqs, &frequencies );

	tstart = dtime();

	for(int i=0; i<nfreqs; i++)
    {
        /* Process one frequency at a time */
        waveletFreq = frequencies[i];

        /* Space delta, 16 grid point per Hz */
        dx = vmin / (6.0 * 2.5 * waveletFreq);
        dy = vmin / (6.0 * 2.5 * waveletFreq);
        dz = vmin / (6.0 * 2.5 * waveletFreq);
   
    	/* number of cells along axis */
    	dimmz = roundup( ceil( lenz / dz ) + 2*HALO, HALO);
		dimmy = roundup( ceil( leny / dy ) + 2*HALO, HALO);
		dimmx = roundup( ceil( lenx / dx ) + 2*HALO, HALO);
	
		/* Check dimensions against hardcoded values */
		if ((dimmz != NZF) || (dimmx != NXF) || (dimmy != NYF)){
			print_error("Hardcoded values does not match input values!\n");
			abort();
		}


    	/* compute delta T */
        dts = 1./(2.5*waveletFreq)/10;
        cfl = (dimmy>1)?0.45:0.55;
        dt = cfl * dx / vmin;
        if(dts < dt) { dt = dts; }
        dt = floorf(dt * 0.9f * 1000000) / 1000000;

    	/* dynamic I/O */
     	stacki = 1; // floor(  0.25 / (2.5 * waveletFreq * dt) );

    	/* compute time number steps requiered */
        forw_steps = max_int( IT_FACTOR * (srclen/dt), 1);
        back_steps = max_int( IT_FACTOR * (rcvlen/dt), 1);

    	for(int grad = 0; grad < gradient_iterations; grad++) /* inversion iteration */
        {
            print_info("Processing %d-gradient iteration", grad);

            for(int shot = 0; shot < nshots; shot++)
            {
                sprintf(shotfolder, "%s/shot.%05d", outputfolder, shot);
                create_folder( shotfolder );

                store_shot_parameters ( shot, &vmin, &stacki, &dt, &forw_steps, &back_steps, 
                                        &dz, &dx, &dy, 
                                        &dimmz, &dimmx, &dimmy, 
                                        outputfolder);

                kernel( RTM_KERNEL, waveletFreq, shot, outputfolder);

                print_info("\tGradient loop processed for %d-th shot", shot);
            }

         //   gather_shots( outputfolder, nshots);

            for(int test = 0; test < test_iterations; test++)
            {
                print_info("Processing %d-th test iteration", test);

                for(int shot=0; shot<nshots; shot++)
                {
                    sprintf(shotfolder, "%s/shot.%05d", outputfolder, shot);
                    create_folder( shotfolder );

                    store_shot_parameters ( shot, &vmin, &stacki, &dt, &forw_steps, &back_steps, 
                    &dz, &dx, &dy, 
                    &dimmz, &dimmx, &dimmy, 
                    outputfolder);

          //          kernel( FM_KERNEL , waveletFreq, shot, outputfolder);

                    print_info("\tTest loop processed for the %d-th shot\n", shot);
                }
            } /* end of test loop */
        } /* end of gradient loop */
    } /* end of frequency loop */

    tend = dtime() - tstart;

    print_info("Program finished in %lf seconds\n", tend);
    
    return 0;
};
