#ifndef _FWI_PROPAGATOR_H_
#define _FWI_PROPAGATOR_H_

#include "fwi_common.h"

/* volume domain dimensions */
#define NZ0 0
#define NX0 0
#define NY0 0
#define NZF 12
#define NXF 12
#define NYF 12

/* row and plane elements */
#define ROW_ELEMENTS       NZF
#define PLANE_ELEMENTS     NXF * NZF

/* number of fields within a cell_t structure */
#define N_CELL_FIELDS 58 

/* a cell contains the scalar value for each one of the 58 fields */
typedef real_t cell_t[N_CELL_FIELDS];

/* define indices for cell_t structure */
typedef enum{
	tl_u   , // 0
	tl_v   , // 1
	tl_w   , // 2
	tr_u   , // 3
	tr_v   , // 4
	tr_w   , // 5
	bl_u   , // 6
	bl_v   , // 7
	bl_w   , // 8
	br_u   , // 9
	br_v   , // 10
	br_w   , // 11
	tl_zz  , // 12
	tl_xz  , // 13
	tl_yz  , // 14
	tl_xx  , // 15
	tl_xy  , // 16
	tl_yy  , // 17
	tr_zz  , // 18
	tr_xz  , // 19
	tr_yz  , // 20
	tr_xx  , // 21
	tr_xy  , // 22
	tr_yy  , // 23
	bl_zz  , // 24
	bl_xz  , // 25
	bl_yz  , // 26
	bl_xx  , // 27
	bl_xy  , // 28
	bl_yy  , // 29
	br_zz  , // 30
	br_xz  , // 31
	br_yz  , // 32
	br_xx  , // 33
	br_xy  , // 34
	br_yy  , // 35
	C11    , // 36
	C12    , // 37
	C13    , // 38
	C14    , // 39
	C15    , // 40
	C16    , // 41
	C22    , // 42
	C23    , // 43
	C24    , // 44
	C25    , // 45
	C26    , // 46
	C33    , // 47
	C34    , // 48
	C35    , // 49
	C36    , // 50
	C44    , // 51
	C45    , // 52
	C46    , // 53
	C55    , // 54
	C56    , // 55
	C66    , // 56
	Rho      // 57
} field_t;

/* define row, plane and volume */
typedef cell_t row_t[NZF] __attribute__((aligned(4)));
typedef row_t plane_t[NXF];
typedef plane_t (*restrict volume_t);


#define C0  (    1.196289f)      
#define C1  (-7.975260e-2f)
#define C2  ( 9.570313e-3f)  
#define C3  (-6.975446e-4f) 

#define EXP_TL 0.25f
#define EXP_TR 0.0625f
#define EXP_BL 0.0625f
#define EXP_BR 0.0625f

#define ASSUMED_DISTANCE 16

typedef enum {back_offset, forw_offset} offset_t;

real_t stencil_Z (volume_t volume,
				const offset_t  off,
				const field_t   idx,
                const real_t    dz,
                const integer   y,
                const integer   x,
                const integer   z);

real_t stencil_X(volume_t volume,
			   const offset_t  off,
			   const field_t   idx,
               const real_t    dx,
               const integer   y,
               const integer   x,
               const integer   z);

real_t stencil_Y(volume_t volume,
			   const offset_t  off,
			   const field_t   idx,
               const real_t    dy,
               const integer   y,
               const integer   x,
               const integer   z);

real_t rho_BL (volume_t volume,
              const integer y,
              const integer x,
              const integer z);

real_t rho_TR (volume_t volume,
              const integer y,
              const integer x,
              const integer z);

real_t rho_BR (volume_t volume,
              const integer y,
              const integer x,
              const integer z);

real_t rho_TL (volume_t volume,
              const integer y,
              const integer x,
              const integer z);

void compute_component_vcell_TL (volume_t       volume,
								const field_t   VelField,
								const field_t   zStressField,
								const field_t   xStressField,
								const field_t   yStressField,
                                const real_t    dt,
                                const real_t    dy,
                                const real_t    dx,
                                const real_t    dz,
                                const offset_t _SY,
                                const offset_t _SX,
                                const offset_t _SZ,
								const integer  y0,
								const integer  yf);

void compute_component_vcell_TR (volume_t       volume,
								const field_t   VelField,
								const field_t   zStressField,
								const field_t   xStressField,
								const field_t   yStressField,
                                const real_t    dt,
                                const real_t    dy,
                                const real_t    dx,
                                const real_t    dz,
                                const offset_t _SY,
                                const offset_t _SX,
                                const offset_t _SZ,
								const integer  y0,
								const integer  yf);

void compute_component_vcell_BL (volume_t       volume,
								const field_t   VelField,
								const field_t   zStressField,
								const field_t   xStressField,
								const field_t   yStressField,
                                const real_t    dt,
                                const real_t    dy,
                                const real_t    dx,
                                const real_t    dz,
                                const offset_t _SY,
                                const offset_t _SX,
                                const offset_t _SZ,
								const integer  y0,
								const integer  yf);

void compute_component_vcell_BR (volume_t       volume,
								const field_t   VelField,
								const field_t   zStressField,
								const field_t   xStressField,
								const field_t   yStressField,
                                const real_t    dt,
                                const real_t    dy,
                                const real_t    dx,
                                const real_t    dz,
                                const offset_t _SY,
                                const offset_t _SX,
                                const offset_t _SZ,
								const integer  y0,
								const integer  yf);

void velocity_propagator(volume_t         volume,
                         const real_t      dt,
                         const real_t      dy,
                         const real_t      dx,
                         const real_t      dz,
						 const integer     y0,
						 const integer     yf);

/* ------------------------------------------------------------------------------ */
/*                                                                                */
/*                               CALCULO DE TENSIONES                             */
/*                                                                                */
/* ------------------------------------------------------------------------------ */

real_t cell_coeff_BR ( volume_t volume,
		const field_t idx,
		const integer y,
		const integer x,
		const integer z);

real_t cell_coeff_TL (volume_t volume,
		const field_t idx,
		const integer y,
		const integer x,
		const integer z);

real_t cell_coeff_BL (volume_t volume,
		const field_t idx,
		const integer y,
		const integer x,
		const integer z);

real_t cell_coeff_TR (volume_t volume,
		const field_t idx,
		const integer y,
		const integer x,
		const integer z);

real_t cell_coeff_ARTM_BR(volume_t volume,
		const field_t idx,
		const integer y,
		const integer x,
		const integer z);

real_t cell_coeff_ARTM_TL(volume_t volume,
		const field_t idx,
		const integer y,
		const integer x,
		const integer z);

real_t cell_coeff_ARTM_BL(volume_t volume,
		const field_t idx,
		const integer y,
		const integer x,
		const integer z);

real_t cell_coeff_ARTM_TR(volume_t volume,
		const field_t idx,
		const integer y,
		const integer x,
		const integer z);

void stress_update (volume_t volume,
					const field_t stressField,
					const field_t c1,
					const field_t c2,
					const field_t c3,
					const field_t c4,
					const field_t c5,
					const field_t c6,
					const integer y,
					const integer x,
					const integer z,
					const real_t  dt,
					const real_t  u_x,
					const real_t  u_y,
					const real_t  u_z,
					const real_t  v_x,
					const real_t  v_y,
					const real_t  v_z,
					const real_t  w_x,
					const real_t  w_y,
					const real_t  w_z);

void compute_component_scell_TR (volume_t     volume,
								const field_t vx_u,
								const field_t vx_v,
								const field_t vx_w,
								const field_t vy_u,
								const field_t vy_v,
								const field_t vy_w,
								const field_t vz_u,
								const field_t vz_v,
								const field_t vz_w,
								const real_t  dt,
								const real_t  dy,
								const real_t  dx,
								const real_t  dz,
								const offset_t _SY,
								const offset_t _SX,
								const offset_t _SZ,
								const integer  y0,
								const integer  yf);

void compute_component_scell_TL (volume_t     volume,
								const field_t vx_u,
								const field_t vx_v,
								const field_t vx_w,
								const field_t vy_u,
								const field_t vy_v,
								const field_t vy_w,
								const field_t vz_u,
								const field_t vz_v,
								const field_t vz_w,
								const real_t  dt,
								const real_t  dy,
								const real_t  dx,
								const real_t  dz,
								const offset_t _SY,
								const offset_t _SX,
								const offset_t _SZ,
								const integer  y0,
								const integer  yf);

void compute_component_scell_BR (volume_t     volume,
								const field_t vx_u,
								const field_t vx_v,
								const field_t vx_w,
								const field_t vy_u,
								const field_t vy_v,
								const field_t vy_w,
								const field_t vz_u,
								const field_t vz_v,
								const field_t vz_w,
								const real_t  dt,
								const real_t  dy,
								const real_t  dx,
								const real_t  dz,
								const offset_t _SY,
								const offset_t _SX,
								const offset_t _SZ,
								const integer  y0,
								const integer  yf);

void compute_component_scell_BL (volume_t     volume,
								const field_t vx_u,
								const field_t vx_v,
								const field_t vx_w,
								const field_t vy_u,
								const field_t vy_v,
								const field_t vy_w,
								const field_t vz_u,
								const field_t vz_v,
								const field_t vz_w,
								const real_t  dt,
								const real_t  dy,
								const real_t  dx,
								const real_t  dz,
								const offset_t _SY,
								const offset_t _SX,
								const offset_t _SZ,
								const integer  y0,
								const integer  yf);


void stress_propagator( volume_t     volume,
                       const real_t    dt,
                       const real_t    dy,
                       const real_t    dx,
                       const real_t    dz,
					   const integer   y0,
					   const integer   yf);

void source_insertion ( volume_t        v,
                        const real_t    dt,
                        const real_t    src, 
                        const real_t    dy,
                        const real_t    dx,
                        const real_t    dz);

#endif /* end of _FWI_PROPAGATOR_H_ definition */
