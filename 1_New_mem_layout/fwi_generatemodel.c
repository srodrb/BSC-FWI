/*
 * =====================================================================================
 *
 *       Filename:  GenerateInputModel.c
 *
 *    Description:  Generates input velocity model for the FWI code
 *
 *        Version:  1.0
 *        Created:  26/01/16 11:55:02
 *       Revision:  none
 *       Compiler:  icc
 *
 *         Author:  YOUR NAME (),
 *   Organization:
 *
 * =====================================================================================
 */

#include "fwi_propagator.h"
// #include "fwi_kernel.h"


int main(int argc, const char *argv[])
{
  /* set seed for random number generator */
  srand(314);

  	/* local vairables */
  	real_t lenz,lenx,leny,vmin,srclen,rcvlen;
  	real_t dx, dy, dz;
  	integer dimmz, dimmx, dimmy, numberOfCells;
	int nfreqs;
	real_t *frequencies, *buffer;
	real_t waveletFreq;
	FILE *model;
  	char outputfolder[200], modelname[300];

	print_debug("Loading parameter from %s file\n", argv[1]);
  	read_simulation_parameters( argv[1], &lenz, &lenx, &leny, &vmin, &srclen, &rcvlen, outputfolder);
	read_freqlist( argv[2], &nfreqs, &frequencies);

  for(int i=0; i<nfreqs; i++)
  {
	waveletFreq = frequencies[i];
	print_info("Creating synthetic velocity input model for %f Hz freq\n", waveletFreq );

    /* compute discretization deltas, 16 == puntos por longitud de onda */
    dx = vmin / (6.0 * 2.5 * waveletFreq);
    dy = vmin / (6.0 * 2.5 * waveletFreq);
    dz = vmin / (6.0 * 2.5 * waveletFreq);

    /* number of cells along axis */
    dimmz = roundup( ceil( lenz / dz ) + 2*HALO, HALO);
	dimmy = roundup( ceil( leny / dy ) + 2*HALO, HALO);
	dimmx = roundup( ceil( lenx / dx ) + 2*HALO, HALO);

	numberOfCells = dimmz * dimmy * dimmx;

	print_info("Model dimensions (Z, X, Y): %d %d %d\n", dimmz, dimmx, dimmy);
	print_debug("Elements/array = "I"\n", numberOfCells);
    sprintf( modelname, "../InputModels/velocitymodel_%.2f.bin", waveletFreq );

    /* create velocity model file */
	model = safe_fopen( modelname, "wb");
	
	/* initialize a buffer with the minimum velocity of the model */
	buffer = __malloc( ALIGN_REAL, sizeof(real_t) * numberOfCells);
	for ( int j = 0; j < numberOfCells; j++) { buffer[j] = vmin; } 

	/* write the velocity model file */
	for( int i = 0; i < WRITTEN_FIELDS; i++) {
    	safe_fwrite( buffer, sizeof(real_t), numberOfCells, model);
	}

	/* free buffer */
	__free   ( buffer );

	/*  close model file */
	safe_fclose( modelname, model);

	print_info("Model %s created correctly\n", modelname);
	}

	__free ( frequencies );
	print_info("End of the program\n");
	return 0;
}
