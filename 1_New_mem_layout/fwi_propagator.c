#include "fwi_propagator.h"

real_t stencil_Z (volume_t volume,
				const offset_t   off,
				const field_t    idx,
                const real_t     dz,
                const integer    y,
                const integer    x,
                const integer    z)
{
  return  ((C0 * ( volume[y][x][z  +off][idx]  - volume[y][x][z-1+off][idx]) +
            C1 * ( volume[y][x][z+1+off][idx]  - volume[y][x][z-2+off][idx]) +
            C2 * ( volume[y][x][z+2+off][idx]  - volume[y][x][z-3+off][idx]) +
            C3 * ( volume[y][x][z+3+off][idx]  - volume[y][x][z-4+off][idx])) * dz );
};

real_t stencil_X(volume_t volume,
				const offset_t off,
				const field_t idx,
                const real_t dx,
                const integer y,
                const integer x,
                const integer z)
{
	return ( (  C0 * ( volume[y][x  +off][z][idx]  - volume[y][x-1+off][z][idx]) +
                C1 * ( volume[y][x+1+off][z][idx]  - volume[y][x-2+off][z][idx]) +
                C2 * ( volume[y][x+2+off][z][idx]  - volume[y][x-3+off][z][idx]) +
                C3 * ( volume[y][x+3+off][z][idx]  - volume[y][x-4+off][z][idx])) * dx );
};

real_t stencil_Y(volume_t volume,
				const offset_t off,
				const field_t idx,
                const real_t dy,
                const integer y,
                const integer x,
                const integer z)
{
  return ( (C0 * ( volume[y  +off][x][z][idx]  - volume[y-1+off][x][z][idx]) +
            C1 * ( volume[y+1+off][x][z][idx]  - volume[y-2+off][x][z][idx]) +
            C2 * ( volume[y+2+off][x][z][idx]  - volume[y-3+off][x][z][idx]) +
            C3 * ( volume[y+3+off][x][z][idx]  - volume[y-4+off][x][z][idx])) * dy );
};

/* -------------------------------------------------------------------- */
/*                     KERNELS FOR VELOCITY                             */
/* -------------------------------------------------------------------- */


real_t rho_BL (volume_t volume,
			const integer y,
            const integer x,
            const integer z)
{
    return (2.0f / (volume[y][x][z][Rho] + volume[y][x][z+1][Rho]));
};

real_t rho_TR (volume_t volume,
			const integer y,
            const integer x,
            const integer z)
{
    return (2.0f/ (volume[y][x][z][Rho] + volume[y][x+1][z][Rho]) );
};

real_t rho_BR (volume_t volume,
			const integer y,
            const integer x,
            const integer z)
{
    return ( 8.0f/ (volume[y  ][x  ][z  ][Rho] +
                    volume[y  ][x  ][z+1][Rho] +
                    volume[y  ][x+1][z  ][Rho] +
                    volume[y+1][x  ][z  ][Rho] +
                    volume[y+1][x+1][z  ][Rho] +
                    volume[y  ][x+1][z+1][Rho] +
                    volume[y+1][x  ][z+1][Rho] +
                    volume[y+1][x+1][z+1][Rho]) );
};

real_t rho_TL (volume_t volume,
			  const integer y,
              const integer x,
              const integer z)
{
    return (2.0f/ (volume[y][x][z][Rho] + volume[y+1][x][z][Rho]));
};

void compute_component_vcell_TL (volume_t       volume,
								const field_t   VelField,
								const field_t   zStressField,
								const field_t   xStressField,
								const field_t   yStressField,
                                const real_t    dt,
                                const real_t    dy,
                                const real_t    dx,
                                const real_t    dz,
                                const offset_t _SY,
                                const offset_t _SX,
                                const offset_t _SZ,
								const integer  y0,
								const integer  yf)
{
    for(integer y=y0; y < yf; y++) {
        for(integer x=HALO; x < (NXF - HALO); x++) {
            for(integer z=HALO; z < (NZF - HALO); z++) {
                const real_t lrho = rho_TL    ( volume, y, x, z);
                const real_t stx  = stencil_X ( volume, _SX, xStressField, dx, y, x, z);
                const real_t sty  = stencil_Y ( volume, _SY, yStressField, dy, y, x, z);
                const real_t stz  = stencil_Z ( volume, _SZ, zStressField, dz, y, x, z);

				volume[y][x][z][VelField] += ((stx + sty + stz) * dt * lrho);
            }
        }
    }
};

void compute_component_vcell_TR (volume_t       volume,
								const field_t   VelField,
								const field_t   zStressField,
								const field_t   xStressField,
								const field_t   yStressField,
                                const real_t    dt,
                                const real_t    dy,
                                const real_t    dx,
                                const real_t    dz,
                                const offset_t _SY,
                                const offset_t _SX,
                                const offset_t _SZ,
								const integer  y0,
								const integer  yf)
{
    for(integer y=y0; y < yf; y++) {
        for(integer x=HALO; x < (NXF - HALO); x++) {
            for(integer z=HALO; z < (NZF - HALO); z++) {
                const real_t lrho = rho_TR    ( volume, y, x, z);
                const real_t stx  = stencil_X ( volume, _SX, xStressField, dx, y, x, z);
                const real_t sty  = stencil_Y ( volume, _SY, yStressField, dy, y, x, z);
                const real_t stz  = stencil_Z ( volume, _SZ, zStressField, dz, y, x, z);

				volume[y][x][z][VelField] += ((stx + sty + stz) * dt * lrho);
            }
        }
    }
};

void compute_component_vcell_BL (volume_t       volume,
								const field_t   VelField,
								const field_t   zStressField,
								const field_t   xStressField,
								const field_t   yStressField,
                                const real_t    dt,
                                const real_t    dy,
                                const real_t    dx,
                                const real_t    dz,
                                const offset_t _SY,
                                const offset_t _SX,
                                const offset_t _SZ,
								const integer  y0,
								const integer  yf)
{
    for(integer y=y0; y < yf; y++) {
        for(integer x=HALO; x < (NXF - HALO); x++) {
            for(integer z=HALO; z < (NZF - HALO); z++) {
                const real_t lrho = rho_BL    ( volume, y, x, z);
                const real_t stx  = stencil_X ( volume, _SX, xStressField, dx, y, x, z);
                const real_t sty  = stencil_Y ( volume, _SY, yStressField, dy, y, x, z);
                const real_t stz  = stencil_Z ( volume, _SZ, zStressField, dz, y, x, z);

				volume[y][x][z][VelField] += ((stx + sty + stz) * dt * lrho);
            }
        }
    }
};



void compute_component_vcell_BR (volume_t       volume,
								const field_t   VelField,
								const field_t   zStressField,
								const field_t   xStressField,
								const field_t   yStressField,
                                const real_t    dt,
                                const real_t    dy,
                                const real_t    dx,
                                const real_t    dz,
                                const offset_t _SY,
                                const offset_t _SX,
                                const offset_t _SZ,
								const integer  y0,
								const integer  yf)
{
    for(integer y=y0; y < yf; y++) {
        for(integer x=HALO; x < (NXF - HALO); x++) {
            for(integer z=HALO; z < (NZF - HALO); z++) {
                const real_t lrho = rho_BR    ( volume, y, x, z);
                const real_t stx  = stencil_X ( volume, _SX, xStressField, dx, y, x, z);
                const real_t sty  = stencil_Y ( volume, _SY, yStressField, dy, y, x, z);
                const real_t stz  = stencil_Z ( volume, _SZ, zStressField, dz, y, x, z);

				volume[y][x][z][VelField] += ((stx + sty + stz) * dt * lrho);
            }
        }
    }
};

void velocity_propagator(volume_t         volume,
                        const real_t      dt,
                        const real_t      dy,
                        const real_t      dx,
                        const real_t      dz,
						const integer     y0,
						const integer     yf)
{
	#pragma forceinline recursive
    {
		compute_component_vcell_TL (volume, tl_w, bl_zz, tr_xz, tl_yz, dt, dy, dx, dz, back_offset, back_offset, forw_offset, y0, yf);
		compute_component_vcell_TR (volume, tr_w, br_zz, tl_xz, tr_yz, dt, dy, dx, dz, back_offset, forw_offset, back_offset, y0, yf);
		compute_component_vcell_BL (volume, bl_w, tl_zz, br_xz, bl_yz, dt, dy, dx, dz, forw_offset, back_offset, back_offset, y0, yf);
		compute_component_vcell_BR (volume, br_w, tr_zz, bl_xz, br_yz, dt, dy, dx, dz, forw_offset, forw_offset, forw_offset, y0, yf);
		compute_component_vcell_TL (volume, tl_u, bl_xz, tr_xx, tl_xy, dt, dy, dx, dz, back_offset, back_offset, forw_offset, y0, yf);
		compute_component_vcell_TR (volume, tr_u, br_xz, tl_xx, tr_xy, dt, dy, dx, dz, back_offset, forw_offset, back_offset, y0, yf);
		compute_component_vcell_BL (volume, bl_u, tl_xz, br_xx, bl_xy, dt, dy, dx, dz, forw_offset, back_offset, back_offset, y0, yf);
		compute_component_vcell_BR (volume, br_u, tr_xz, bl_xx, br_xy, dt, dy, dx, dz, forw_offset, forw_offset, forw_offset, y0, yf);
		compute_component_vcell_TL (volume, tl_v, bl_yz, tr_xy, tl_yy, dt, dy, dx, dz, back_offset, back_offset, forw_offset, y0, yf);
		compute_component_vcell_TR (volume, tr_v, br_yz, tl_xy, tr_yy, dt, dy, dx, dz, back_offset, forw_offset, back_offset, y0, yf);
		compute_component_vcell_BL (volume, bl_v, tl_yz, br_xy, bl_yy, dt, dy, dx, dz, forw_offset, back_offset, back_offset, y0, yf);
		compute_component_vcell_BR (volume, br_v, tr_yz, bl_xy, br_yy, dt, dy, dx, dz, forw_offset, forw_offset, forw_offset, y0, yf);
    }
};


/* ------------------------------------------------------------------------------ */
/*                                                                                */
/*                               KERNELS FOR STRESS                               */
/*                                                                                */
/* ------------------------------------------------------------------------------ */



real_t cell_coeff_BR ( volume_t volume,
		const field_t idx,
		const integer y,
		const integer x,
		const integer z)
{
    return ( 1.0f /( 0.25f*(volume[y][x  ][z  ][idx] +
                            volume[y][x+1][z  ][idx] +
                            volume[y][x  ][z+1][idx] +
                            volume[y][x+1][z+1][idx])) );
};

real_t cell_coeff_TL (volume_t volume,
		const field_t idx,
		const integer y,
		const integer x,
		const integer z)
{
    return ( 1.0f / (volume[y][x][z][idx]));
};

real_t cell_coeff_BL (volume_t volume,
		const field_t idx,
		const integer y,
		const integer x,
		const integer z)
{
	return (1.0f/(0.25f*(volume[y  ][x][z  ][idx] +
                         volume[y+1][x][z  ][idx] +
                         volume[y  ][x][z+1][idx] +
                         volume[y+1][x][z+1][idx])) );
};

real_t cell_coeff_TR (volume_t volume,
		const field_t idx,
		const integer y,
		const integer x,
		const integer z)
{
    return (1.0f/ ( 0.25f *(volume[y  ][x  ][z][idx] +
                            volume[y  ][x+1][z][idx] +
                            volume[y+1][x  ][z][idx] +
                            volume[y+1][x+1][z][idx])));
};

real_t cell_coeff_ARTM_BR(volume_t volume,
		const field_t idx,
		const integer y,
		const integer x,
		const integer z)
{
    return ((1.0f / volume[y][x  ][z  ][idx]  +
             1.0f / volume[y][x+1][z  ][idx]  +
             1.0f / volume[y][x  ][z+1][idx]  +
             1.0f / volume[y][x+1][z+1][idx]) * 0.25f);
};

real_t cell_coeff_ARTM_TL(volume_t volume,
		const field_t idx,
		const integer y,
		const integer x,
		const integer z)
{
    return (1.0f / volume[y][x][z][idx]);
};

real_t cell_coeff_ARTM_BL(volume_t volume,
		const field_t idx,
		const integer y,
		const integer x,
		const integer z)
{
    return ((1.0f / volume[y  ][x][z  ][idx]  +
             1.0f / volume[y+1][x][z  ][idx]  +
             1.0f / volume[y  ][x][z+1][idx]  +
             1.0f / volume[y+1][x][z+1][idx]) * 0.25f);
};

real_t cell_coeff_ARTM_TR(volume_t volume,
		const field_t idx,
		const integer y,
		const integer x,
		const integer z)
{
    return ((1.0f / volume[y  ][x  ][z][idx]  +
             1.0f / volume[y  ][x+1][z][idx]  +
             1.0f / volume[y+1][x  ][z][idx]  +
             1.0f / volume[y+1][x+1][z][idx]) * 0.25f);
};

void stress_update (volume_t volume,
					const field_t stressField,
					const field_t c1,
					const field_t c2,
					const field_t c3,
					const field_t c4,
					const field_t c5,
					const field_t c6,
					const integer y,
					const integer x,
					const integer z,
					const real_t  dt,
					const real_t  u_x,
					const real_t  u_y,
					const real_t  u_z,
					const real_t  v_x,
					const real_t  v_y,
					const real_t  v_z,
					const real_t  w_x,
					const real_t  w_y,
					const real_t  w_z)
{
	volume[y][x][z][stressField] += dt * c1 * u_x;
	volume[y][x][z][stressField] += dt * c2 * v_y;
	volume[y][x][z][stressField] += dt * c3 * w_z;
	volume[y][x][z][stressField] += dt * c4 * (w_y + v_z);
	volume[y][x][z][stressField] += dt * c5 * (w_x + u_z);
	volume[y][x][z][stressField] += dt * c6 * (v_x + u_y);
};



void compute_component_scell_TR (volume_t     volume,
								const field_t vx_u,
								const field_t vx_v,
								const field_t vx_w,
								const field_t vy_u,
								const field_t vy_v,
								const field_t vy_w,
								const field_t vz_u,
								const field_t vz_v,
								const field_t vz_w,
								const real_t  dt,
								const real_t  dy,
								const real_t  dx,
								const real_t  dz,
								const offset_t _SY,
								const offset_t _SX,
								const offset_t _SZ,
								const integer  y0,
								const integer  yf)
{
    for(integer y=y0; y < yf; y++) {
        for(integer x=HALO; x < (NXF - HALO); x++) {
            for(integer z=HALO; z < (NZF - HALO); z++) {
				const real_t c11 = cell_coeff_TR      (volume, C11, y, x, z);
				const real_t c12 = cell_coeff_TR      (volume, C12, y, x, z);
				const real_t c13 = cell_coeff_TR      (volume, C13, y, x, z);
				const real_t c14 = cell_coeff_ARTM_TR (volume, C14, y, x, z);
				const real_t c15 = cell_coeff_ARTM_TR (volume, C15, y, x, z);
				const real_t c16 = cell_coeff_ARTM_TR (volume, C16, y, x, z);
				const real_t c22 = cell_coeff_TR      (volume, C22, y, x, z);
				const real_t c23 = cell_coeff_TR      (volume, C23, y, x, z);
				const real_t c24 = cell_coeff_ARTM_TR (volume, C24, y, x, z);
				const real_t c25 = cell_coeff_ARTM_TR (volume, C25, y, x, z);
				const real_t c26 = cell_coeff_ARTM_TR (volume, C26, y, x, z);
				const real_t c33 = cell_coeff_TR      (volume, C33, y, x, z);
				const real_t c34 = cell_coeff_ARTM_TR (volume, C34, y, x, z);
				const real_t c35 = cell_coeff_ARTM_TR (volume, C35, y, x, z);
				const real_t c36 = cell_coeff_ARTM_TR (volume, C36, y, x, z);
				const real_t c44 = cell_coeff_TR      (volume, C44, y, x, z);
				const real_t c45 = cell_coeff_ARTM_TR (volume, C45, y, x, z);
				const real_t c46 = cell_coeff_ARTM_TR (volume, C46, y, x, z);
				const real_t c55 = cell_coeff_TR      (volume, C55, y, x, z);
				const real_t c56 = cell_coeff_ARTM_TR (volume, C56, y, x, z);
				const real_t c66 = cell_coeff_TR      (volume, C66, y, x, z);

				const real_t u_x = stencil_X (volume, _SX, vx_u, dx, y, x, z);
				const real_t v_x = stencil_X (volume, _SX, vx_v, dx, y, x, z);
				const real_t w_x = stencil_X (volume, _SX, vx_w, dx, y, x, z);

				const real_t u_y = stencil_Y (volume, _SY, vy_u, dy, y, x, z);
				const real_t v_y = stencil_Y (volume, _SY, vy_v, dy, y, x, z);
				const real_t w_y = stencil_Y (volume, _SY, vy_w, dy, y, x, z);

				const real_t u_z = stencil_Z (volume, _SZ, vz_u, dz, y, x, z);
				const real_t v_z = stencil_Z (volume, _SZ, vz_v, dz, y, x, z);
				const real_t w_z = stencil_Z (volume, _SZ, vz_w, dz, y, x, z);

				stress_update (volume, tr_xx, C11, C12, C13, C14, C15, C16, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, tr_yy, C12, C22, C23, C24, C25, C26, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, tr_zz, C13, C23, C33, C34, C35, C36, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, tr_yz, C14, C24, C34, C44, C45, C46, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, tr_xz, C15, C25, C35, C45, C55, C56, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, tr_xy, C16, C26, C36, C46, C56, C66, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
			}
		}
	}
};

void compute_component_scell_TL (volume_t     volume,
								const field_t vx_u,
								const field_t vx_v,
								const field_t vx_w,
								const field_t vy_u,
								const field_t vy_v,
								const field_t vy_w,
								const field_t vz_u,
								const field_t vz_v,
								const field_t vz_w,
								const real_t  dt,
								const real_t  dy,
								const real_t  dx,
								const real_t  dz,
								const offset_t _SY,
								const offset_t _SX,
								const offset_t _SZ,
								const integer  y0,
								const integer  yf)
{
    for(integer y=y0; y < yf; y++) {
        for(integer x=HALO; x < (NXF - HALO); x++) {
            for(integer z=HALO; z < (NZF - HALO); z++) {
				const real_t c11 = cell_coeff_TL      (volume, C11, y, x, z);
				const real_t c12 = cell_coeff_TL      (volume, C12, y, x, z);
				const real_t c13 = cell_coeff_TL      (volume, C13, y, x, z);
				const real_t c14 = cell_coeff_ARTM_TL (volume, C14, y, x, z);
				const real_t c15 = cell_coeff_ARTM_TL (volume, C15, y, x, z);
				const real_t c16 = cell_coeff_ARTM_TL (volume, C16, y, x, z);
				const real_t c22 = cell_coeff_TL      (volume, C22, y, x, z);
				const real_t c23 = cell_coeff_TL      (volume, C23, y, x, z);
				const real_t c24 = cell_coeff_ARTM_TL (volume, C24, y, x, z);
				const real_t c25 = cell_coeff_ARTM_TL (volume, C25, y, x, z);
				const real_t c26 = cell_coeff_ARTM_TL (volume, C26, y, x, z);
				const real_t c33 = cell_coeff_TL      (volume, C33, y, x, z);
				const real_t c34 = cell_coeff_ARTM_TL (volume, C34, y, x, z);
				const real_t c35 = cell_coeff_ARTM_TL (volume, C35, y, x, z);
				const real_t c36 = cell_coeff_ARTM_TL (volume, C36, y, x, z);
				const real_t c44 = cell_coeff_TL      (volume, C44, y, x, z);
				const real_t c45 = cell_coeff_ARTM_TL (volume, C45, y, x, z);
				const real_t c46 = cell_coeff_ARTM_TL (volume, C46, y, x, z);
				const real_t c55 = cell_coeff_TL      (volume, C55, y, x, z);
				const real_t c56 = cell_coeff_ARTM_TL (volume, C56, y, x, z);
				const real_t c66 = cell_coeff_TL      (volume, C66, y, x, z);

				const real_t u_x = stencil_X (volume, _SX, vx_u, dx, y, x, z);
				const real_t v_x = stencil_X (volume, _SX, vx_v, dx, y, x, z);
				const real_t w_x = stencil_X (volume, _SX, vx_w, dx, y, x, z);

				const real_t u_y = stencil_Y (volume, _SY, vy_u, dy, y, x, z);
				const real_t v_y = stencil_Y (volume, _SY, vy_v, dy, y, x, z);
				const real_t w_y = stencil_Y (volume, _SY, vy_w, dy, y, x, z);

				const real_t u_z = stencil_Z (volume, _SZ, vz_u, dz, y, x, z);
				const real_t v_z = stencil_Z (volume, _SZ, vz_v, dz, y, x, z);
				const real_t w_z = stencil_Z (volume, _SZ, vz_w, dz, y, x, z);
				
				stress_update (volume, tr_xx, C11, C12, C13, C14, C15, C16, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, tr_yy, C12, C22, C23, C24, C25, C26, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, tr_zz, C13, C23, C33, C34, C35, C36, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, tr_yz, C14, C24, C34, C44, C45, C46, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, tr_xz, C15, C25, C35, C45, C55, C56, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, tr_xy, C16, C26, C36, C46, C56, C66, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
			}
		}
	}
};

void compute_component_scell_BR (volume_t     volume,
								const field_t vx_u,
								const field_t vx_v,
								const field_t vx_w,
								const field_t vy_u,
								const field_t vy_v,
								const field_t vy_w,
								const field_t vz_u,
								const field_t vz_v,
								const field_t vz_w,
								const real_t  dt,
								const real_t  dy,
								const real_t  dx,
								const real_t  dz,
								const offset_t _SY,
								const offset_t _SX,
								const offset_t _SZ,
								const integer  y0,
								const integer  yf)
{
    for(integer y=y0; y < yf; y++) {
        for(integer x=HALO; x < (NXF - HALO); x++) {
            for(integer z=HALO; z < (NZF - HALO); z++) {
				const real_t c11 = cell_coeff_BR      (volume, C11, y, x, z);
				const real_t c12 = cell_coeff_BR      (volume, C12, y, x, z);
				const real_t c13 = cell_coeff_BR      (volume, C13, y, x, z);
				const real_t c22 = cell_coeff_BR      (volume, C22, y, x, z);
				const real_t c23 = cell_coeff_BR      (volume, C23, y, x, z);
				const real_t c33 = cell_coeff_BR      (volume, C33, y, x, z);
				const real_t c44 = cell_coeff_BR      (volume, C44, y, x, z);
				const real_t c55 = cell_coeff_BR      (volume, C55, y, x, z);
				const real_t c66 = cell_coeff_BR      (volume, C66, y, x, z);
				const real_t c14 = cell_coeff_ARTM_BR (volume, C14, y, x, z);
				const real_t c15 = cell_coeff_ARTM_BR (volume, C15, y, x, z);
				const real_t c16 = cell_coeff_ARTM_BR (volume, C16, y, x, z);
				const real_t c24 = cell_coeff_ARTM_BR (volume, C24, y, x, z);
				const real_t c25 = cell_coeff_ARTM_BR (volume, C25, y, x, z);
				const real_t c26 = cell_coeff_ARTM_BR (volume, C26, y, x, z);
				const real_t c34 = cell_coeff_ARTM_BR (volume, C34, y, x, z);
				const real_t c35 = cell_coeff_ARTM_BR (volume, C35, y, x, z);
				const real_t c36 = cell_coeff_ARTM_BR (volume, C36, y, x, z);
				const real_t c45 = cell_coeff_ARTM_BR (volume, C45, y, x, z);
				const real_t c46 = cell_coeff_ARTM_BR (volume, C46, y, x, z);
				const real_t c56 = cell_coeff_ARTM_BR (volume, C56, y, x, z);
		
				const real_t u_x = stencil_X (volume, _SX, vx_u, dx, y, x, z);
				const real_t v_x = stencil_X (volume, _SX, vx_v, dx, y, x, z);
				const real_t w_x = stencil_X (volume, _SX, vx_w, dx, y, x, z);

				const real_t u_y = stencil_Y (volume, _SY, vy_u, dy, y, x, z);
				const real_t v_y = stencil_Y (volume, _SY, vy_v, dy, y, x, z);
				const real_t w_y = stencil_Y (volume, _SY, vy_w, dy, y, x, z);
				
				const real_t u_z = stencil_Z (volume, _SZ, vz_u, dz, y, x, z);
				const real_t v_z = stencil_Z (volume, _SZ, vz_v, dz, y, x, z);
				const real_t w_z = stencil_Z (volume, _SZ, vz_w, dz, y, x, z);

				stress_update (volume, tr_xx, C11, C12, C13, C14, C15, C16, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, tr_yy, C12, C22, C23, C24, C25, C26, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, tr_zz, C13, C23, C33, C34, C35, C36, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, tr_yz, C14, C24, C34, C44, C45, C46, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, tr_xz, C15, C25, C35, C45, C55, C56, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, tr_xy, C16, C26, C36, C46, C56, C66, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
			}
		}
	}
};

void compute_component_scell_BL (volume_t     volume,
								const field_t vx_u,
								const field_t vx_v,
								const field_t vx_w,
								const field_t vy_u,
								const field_t vy_v,
								const field_t vy_w,
								const field_t vz_u,
								const field_t vz_v,
								const field_t vz_w,
								const real_t  dt,
								const real_t  dy,
								const real_t  dx,
								const real_t  dz,
								const offset_t _SZ,
								const offset_t _SX,
								const offset_t _SY,
								const integer  y0,
								const integer  yf)
{
    for(integer y=y0; y < yf; y++) {
        for(integer x=HALO; x < (NXF - HALO); x++) {
            for(integer z=HALO; z < (NZF - HALO); z++) {
				const real_t c11 = cell_coeff_BL      (volume, C11, y, x, z);
				const real_t c12 = cell_coeff_BL      (volume, C12, y, x, z);
				const real_t c13 = cell_coeff_BL      (volume, C13, y, x, z);
				const real_t c14 = cell_coeff_ARTM_BL (volume, C14, y, x, z);
				const real_t c15 = cell_coeff_ARTM_BL (volume, C15, y, x, z);
				const real_t c16 = cell_coeff_ARTM_BL (volume, C16, y, x, z);
				const real_t c22 = cell_coeff_BL      (volume, C22, y, x, z);
				const real_t c23 = cell_coeff_BL      (volume, C23, y, x, z);
				const real_t c24 = cell_coeff_ARTM_BL (volume, C24, y, x, z);
				const real_t c25 = cell_coeff_ARTM_BL (volume, C25, y, x, z);
				const real_t c26 = cell_coeff_ARTM_BL (volume, C26, y, x, z);
				const real_t c33 = cell_coeff_BL      (volume, C33, y, x, z);
				const real_t c34 = cell_coeff_ARTM_BL (volume, C34, y, x, z);
				const real_t c35 = cell_coeff_ARTM_BL (volume, C35, y, x, z);
				const real_t c36 = cell_coeff_ARTM_BL (volume, C36, y, x, z);
				const real_t c44 = cell_coeff_BL      (volume, C44, y, x, z);
				const real_t c45 = cell_coeff_ARTM_BL (volume, C45, y, x, z);
				const real_t c46 = cell_coeff_ARTM_BL (volume, C46, y, x, z);
				const real_t c55 = cell_coeff_BL      (volume, C55, y, x, z);
				const real_t c56 = cell_coeff_ARTM_BL (volume, C56, y, x, z);
				const real_t c66 = cell_coeff_BL      (volume, C66, y, x, z);

				const real_t u_x = stencil_X (volume, _SX, vx_u, dx, y, x, z);
				const real_t v_x = stencil_X (volume, _SX, vx_v, dx, y, x, z);
				const real_t w_x = stencil_X (volume, _SX, vx_w, dx, y, x, z);
				
				const real_t u_y = stencil_Y (volume, _SY, vy_u, dy, y, x, z);
				const real_t v_y = stencil_Y (volume, _SY, vy_v, dy, y, x, z);
				const real_t w_y = stencil_Y (volume, _SY, vy_w, dy, y, x, z);

				const real_t u_z = stencil_Z (volume, _SZ, vz_u, dz, y, x, z);
				const real_t v_z = stencil_Z (volume, _SZ, vz_v, dz, y, x, z);
				const real_t w_z = stencil_Z (volume, _SZ, vz_w, dz, y, x, z);
				
				stress_update (volume, bl_xx, C11, C12, C13, C14, C15, C16, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, bl_yy, C12, C22, C23, C24, C25, C26, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, bl_zz, C13, C23, C33, C34, C35, C36, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, bl_yz, C14, C24, C34, C44, C45, C46, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, bl_xz, C15, C25, C35, C45, C55, C56, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (volume, bl_xy, C16, C26, C36, C46, C56, C66, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
			}
		}
	}
};

void stress_propagator( volume_t     volume,
                       const real_t    dt,
                       const real_t    dy,
                       const real_t    dx,
                       const real_t    dz,
					   const integer   y0,
					   const integer   yf)
{
	#pragma forceinline recursive
	{
		compute_component_scell_BR ( volume, tr_u, tr_v, tr_w, bl_u, bl_v, bl_w, br_u, br_v, br_w, dt, dy, dx, dz, forw_offset, back_offset, forw_offset, y0, yf);
		compute_component_scell_BL ( volume, tl_u, tl_v, tl_w, br_u, br_v, br_w, bl_u, bl_v, bl_w, dt, dy, dx, dz, forw_offset, forw_offset, back_offset, y0, yf);
		compute_component_scell_TR ( volume, br_u, br_v, br_w, tl_u, tl_v, tl_w, tr_u, tr_v, tr_w, dt, dy, dx, dz, back_offset, forw_offset, forw_offset, y0, yf);
		compute_component_scell_TL ( volume, bl_u, bl_v, bl_w, tr_u, tr_v, tr_w, tl_u, tl_v, tl_w, dt, dy, dx, dz, back_offset, back_offset, back_offset, y0, yf);
	}
};


void source_insertion ( volume_t         volume,
                        const real_t     dt,
                        const real_t     src, 
                        const real_t     dy,
                        const real_t     dx,
                        const real_t     dz)
{
    real_t src_value;
    integer z, x, y;
  
    /* Source insertion position */
    src_value = src * (dz * dx * ((dy>0)?dy:1));
    z = floorf((NZF-NZ0) / 2) + HALO;
    x = floorf((NXF-NX0) / 2) + HALO;
    y = floorf((NYF-NY0) / 2) + HALO;
    
    print_debug( "Source Insertion at mesh point (%d,%d,%d)\n", z, x, y );

    /* Source insertion */
	volume[y][x][z][tl_xx] += dt * src_value * EXP_TL;
	volume[y][x][z][tl_yy] += dt * src_value * EXP_TL;
	volume[y][x][z][tl_zz] += dt * src_value * EXP_TL;
	
	volume[y][x][z][br_xx] += dt * src_value * EXP_BR;
	volume[y][x][z][br_yy] += dt * src_value * EXP_BR;
	volume[y][x][z][br_zz] += dt * src_value * EXP_BR;

	/* Insertion at (y,x,z-1) */
	volume[y][x][z-1][br_xx] += dt * src_value * EXP_BR;
	volume[y][x][z-1][br_yy] += dt * src_value * EXP_BR;
	volume[y][x][z-1][br_zz] += dt * src_value * EXP_BR;

	/* Insertion at (y, x-1, z) */
	volume[y][x-1][z][br_xx] += dt * src_value * EXP_BR;
	volume[y][x-1][z][br_yy] += dt * src_value * EXP_BR;
	volume[y][x-1][z][br_zz] += dt * src_value * EXP_BR;

	/* Insertion at (y, x-1, z-1) */
   	volume[y][x-1][z][br_xx] += dt * src_value * EXP_BR;
	volume[y][x-1][z][br_yy] += dt * src_value * EXP_BR;
	volume[y][x-1][z][br_zz] += dt * src_value * EXP_BR;
   
	if( y > 0 ) {
		/* Insertion at (y,x,z) */
		volume[y][x][z][tr_xx] += dt * src_value * EXP_TR;
		volume[y][x][z][tr_yy] += dt * src_value * EXP_TR;
		volume[y][x][z][tr_zz] += dt * src_value * EXP_TR;
		
		/* Insertion at (y,x-1,z) */
		volume[y][x-1][z][tr_xx] += dt * src_value * EXP_TR;
		volume[y][x-1][z][tr_yy] += dt * src_value * EXP_TR;
		volume[y][x-1][z][tr_zz] += dt * src_value * EXP_TR;

		/* Insertion at (y-1,x,z) */
		volume[y-1][x][z][tr_xx] += dt * src_value * EXP_TR;
		volume[y-1][x][z][tr_yy] += dt * src_value * EXP_TR;
		volume[y-1][x][z][tr_zz] += dt * src_value * EXP_TR;

		/* Insertion at (y-1,x-1,z) */
		volume[y-1][x-1][z][tr_xx] += dt * src_value * EXP_TR;
		volume[y-1][x-1][z][tr_yy] += dt * src_value * EXP_TR;
		volume[y-1][x-1][z][tr_zz] += dt * src_value * EXP_TR;

		/* Insertion at (y,x,z) */
		volume[y][x][z][bl_xx] += dt * src_value * EXP_BL;
		volume[y][x][z][bl_yy] += dt * src_value * EXP_BL;
		volume[y][x][z][bl_zz] += dt * src_value * EXP_BL;
		
		/* Insertion at (y,x,z-1) */
		volume[y][x][z-1][bl_xx] += dt * src_value * EXP_BL;
		volume[y][x][z-1][bl_yy] += dt * src_value * EXP_BL;
		volume[y][x][z-1][bl_zz] += dt * src_value * EXP_BL;

		/* Insertion at (y-1,x,z) */
		volume[y-1][x][z][bl_xx] += dt * src_value * EXP_BL;
		volume[y-1][x][z][bl_yy] += dt * src_value * EXP_BL;
		volume[y-1][x][z][bl_zz] += dt * src_value * EXP_BL;

		/* Insertion at (y-1,x,z-1) */
		volume[y-1][x][z-1][bl_xx] += dt * src_value * EXP_BL;
		volume[y-1][x][z-1][bl_yy] += dt * src_value * EXP_BL;
		volume[y-1][x][z-1][bl_zz] += dt * src_value * EXP_BL;
	}
};
