#ifndef _FWI_PROPAGATOR_H_
#define _FWI_PROPAGATOR_H_

#include "fwi_common.h"

/* volume domain dimensions */
#define NZ0 0
#define NX0 0
#define NY0 0
#define NZF 12
#define NXF 12
#define NYF 12

/* row and plane elements */
#define ROW_ELEMENTS       NZF
#define PLANE_ELEMENTS     NXF * NZF


/* define velocity cell structure */
#define V_CELL_FIELDS 13
typedef real_t vcell_t[V_CELL_FIELDS];

/* define velocity cell structure */
#define S_CELL_FIELDS 45
typedef real_t scell_t[S_CELL_FIELDS];

/* define indices for cell_t structure */
typedef enum{
	tl_u   , // 0
	tl_v   , // 1
	tl_w   , // 2
	tr_u   , // 3
	tr_v   , // 4
	tr_w   , // 5
	bl_u   , // 6
	bl_v   , // 7
	bl_w   , // 8
	br_u   , // 9
	br_v   , // 10
	br_w   , // 11
	Rho      // 12
} vfield_t;

typedef enum{
	tl_zz  , // 0
	tl_xz  , // 1
	tl_yz  , // 2
	tl_xx  , // 3
	tl_xy  , // 4
	tl_yy  , // 5
	tr_zz  , // 6
	tr_xz  , // 7
	tr_yz  , // 8
	tr_xx  , // 9
	tr_xy  , // 10
	tr_yy  , // 11
	bl_zz  , // 12
	bl_xz  , // 13
	bl_yz  , // 14
	bl_xx  , // 15
	bl_xy  , // 16
	bl_yy  , // 17
	br_zz  , // 18
	br_xz  , // 19
	br_yz  , // 20
	br_xx  , // 21
	br_xy  , // 22
	br_yy  , // 23
	C11    , // 24
	C12    , // 25
	C13    , // 26
	C14    , // 27
	C15    , // 28
	C16    , // 29
	C22    , // 30
	C23    , // 31
	C24    , // 32
	C25    , // 33
	C26    , // 34
	C33    , // 35
	C34    , // 36
	C35    , // 37
	C36    , // 38
	C44    , // 39
	C45    , // 40
	C46    , // 41
	C55    , // 42
	C56    , // 43
	C66    , // 44
} sfield_t;


/* define row, plane and volume for both velocity and stress cells */
typedef vcell_t vrow_t[NZF] __attribute__((aligned(4)));
typedef vrow_t vplane_t[NXF];
typedef vplane_t (*restrict velocity_t);

typedef scell_t srow_t[NZF] __attribute__((aligned(4)));
typedef srow_t splane_t[NXF];
typedef splane_t (*restrict stress_t);


#define C0  (    1.196289f)      
#define C1  (-7.975260e-2f)
#define C2  ( 9.570313e-3f)  
#define C3  (-6.975446e-4f) 

#define EXP_TL 0.25f
#define EXP_TR 0.0625f
#define EXP_BL 0.0625f
#define EXP_BR 0.0625f

#define ASSUMED_DISTANCE 16

typedef enum {back_offset, forw_offset} offset_t;

real_t v_stencil_Z (velocity_t volume,
				const offset_t  off,
				const vfield_t   idx,
                const real_t    dz,
                const integer   y,
                const integer   x,
                const integer   z);

real_t v_stencil_Y (velocity_t volume,
			   const offset_t  off,
			   const vfield_t   idx,
               const real_t    dx,
               const integer   y,
               const integer   x,
               const integer   z);

real_t v_stencil_X (velocity_t volume,
			   const offset_t  off,
			   const vfield_t   idx,
               const real_t    dy,
               const integer   y,
               const integer   x,
               const integer   z);

real_t s_stencil_Z (stress_t volume,
				const offset_t  off,
				const sfield_t   idx,
                const real_t    dz,
                const integer   y,
                const integer   x,
                const integer   z);

real_t s_stencil_Z (stress_t volume,
			   const offset_t  off,
			   const sfield_t   idx,
               const real_t    dx,
               const integer   y,
               const integer   x,
               const integer   z);

real_t s_stencil_Z (stress_t volume,
			   const offset_t  off,
			   const sfield_t   idx,
               const real_t    dy,
               const integer   y,
               const integer   x,
               const integer   z);


real_t rho_BL (stress_t volume,
              const integer y,
              const integer x,
              const integer z);

real_t rho_TR (stress_t volume,
              const integer y,
              const integer x,
              const integer z);

real_t rho_BR (stress_t volume,
              const integer y,
              const integer x,
              const integer z);

real_t rho_TL (stress_t volume,
              const integer y,
              const integer x,
              const integer z);

void compute_component_vcell_TL (velocity_t       v,
								stress_t        s,
								const vfield_t   VelField,
								const sfield_t   zStressField,
								const sfield_t   xStressField,
								const sfield_t   yStressField,
                                const real_t    dt,
                                const real_t    dy,
                                const real_t    dx,
                                const real_t    dz,
                                const offset_t _SY,
                                const offset_t _SX,
                                const offset_t _SZ,
								const integer  y0,
								const integer  yf);

void compute_component_vcell_TR (velocity_t     v,
								stress_t        s,
								const vfield_t   VelField,
								const sfield_t   zStressField,
								const sfield_t   xStressField,
								const sfield_t   yStressField,
                                const real_t    dt,
                                const real_t    dy,
                                const real_t    dx,
                                const real_t    dz,
                                const offset_t _SY,
                                const offset_t _SX,
                                const offset_t _SZ,
								const integer  y0,
								const integer  yf);

void compute_component_vcell_BL (velocity_t     v,
								stress_t        s,
								const vfield_t   VelField,
								const sfield_t   zStressField,
								const sfield_t   xStressField,
								const sfield_t   yStressField,
                                const real_t    dt,
                                const real_t    dy,
                                const real_t    dx,
                                const real_t    dz,
                                const offset_t _SY,
                                const offset_t _SX,
                                const offset_t _SZ,
								const integer  y0,
								const integer  yf);

void compute_component_vcell_BR (velocity_t     v,
								stress_t        s,
								const vfield_t   VelField,
								const sfield_t   zStressField,
								const sfield_t   xStressField,
								const sfield_t   yStressField,
                                const real_t    dt,
                                const real_t    dy,
                                const real_t    dx,
                                const real_t    dz,
                                const offset_t _SY,
                                const offset_t _SX,
                                const offset_t _SZ,
								const integer  y0,
								const integer  yf);

void velocity_propagator(velocity_t        v,
						 stress_t          s,
                         const real_t      dt,
                         const real_t      dy,
                         const real_t      dx,
                         const real_t      dz,
						 const integer     y0,
						 const integer     yf);

/* ------------------------------------------------------------------------------ */
/*                                                                                */
/*                               CALCULO DE TENSIONES                             */
/*                                                                                */
/* ------------------------------------------------------------------------------ */

real_t cell_coeff_BR ( stress_t volume,
		const sfield_t idx,
		const integer y,
		const integer x,
		const integer z);

real_t cell_coeff_TL (stress_t volume,
		const sfield_t idx,
		const integer y,
		const integer x,
		const integer z);

real_t cell_coeff_BL (stress_t volume,
		const sfield_t idx,
		const integer y,
		const integer x,
		const integer z);

real_t cell_coeff_TR (stress_t volume,
		const sfield_t idx,
		const integer y,
		const integer x,
		const integer z);

real_t cell_coeff_ARTM_BR(stress_t volume,
		const sfield_t idx,
		const integer y,
		const integer x,
		const integer z);

real_t cell_coeff_ARTM_TL(stress_t volume,
		const sfield_t idx,
		const integer y,
		const integer x,
		const integer z);

real_t cell_coeff_ARTM_BL(stress_t volume,
		const sfield_t idx,
		const integer y,
		const integer x,
		const integer z);

real_t cell_coeff_ARTM_TR(stress_t volume,
		const sfield_t idx,
		const integer y,
		const integer x,
		const integer z);

void stress_update (stress_t volume,
					const sfield_t stressField,
					const real_t c1,
					const real_t c2,
					const real_t c3,
					const real_t c4,
					const real_t c5,
					const real_t c6,
					const integer y,
					const integer x,
					const integer z,
					const real_t  dt,
					const real_t  u_x,
					const real_t  u_y,
					const real_t  u_z,
					const real_t  v_x,
					const real_t  v_y,
					const real_t  v_z,
					const real_t  w_x,
					const real_t  w_y,
					const real_t  w_z);

void compute_component_scell_TR (velocity_t   v,
								stress_t      s,
								const vfield_t vx_u,
								const vfield_t vx_v,
								const vfield_t vx_w,
								const vfield_t vy_u,
								const vfield_t vy_v,
								const vfield_t vy_w,
								const vfield_t vz_u,
								const vfield_t vz_v,
								const vfield_t vz_w,
								const real_t  dt,
								const real_t  dy,
								const real_t  dx,
								const real_t  dz,
								const offset_t _SY,
								const offset_t _SX,
								const offset_t _SZ,
								const integer  y0,
								const integer  yf);

void compute_component_scell_TL (velocity_t   v,
								stress_t      s,
								const vfield_t vx_u,
								const vfield_t vx_v,
								const vfield_t vx_w,
								const vfield_t vy_u,
								const vfield_t vy_v,
								const vfield_t vy_w,
								const vfield_t vz_u,
								const vfield_t vz_v,
								const vfield_t vz_w,
								const real_t  dt,
								const real_t  dy,
								const real_t  dx,
								const real_t  dz,
								const offset_t _SY,
								const offset_t _SX,
								const offset_t _SZ,
								const integer  y0,
								const integer  yf);

void compute_component_scell_BR (velocity_t   v,
								stress_t      s,
								const vfield_t vx_u,
								const vfield_t vx_v,
								const vfield_t vx_w,
								const vfield_t vy_u,
								const vfield_t vy_v,
								const vfield_t vy_w,
								const vfield_t vz_u,
								const vfield_t vz_v,
								const vfield_t vz_w,
								const real_t  dt,
								const real_t  dy,
								const real_t  dx,
								const real_t  dz,
								const offset_t _SY,
								const offset_t _SX,
								const offset_t _SZ,
								const integer  y0,
								const integer  yf);

void compute_component_scell_BL (velocity_t   v,
								stress_t      s,
								const vfield_t vx_u,
								const vfield_t vx_v,
								const vfield_t vx_w,
								const vfield_t vy_u,
								const vfield_t vy_v,
								const vfield_t vy_w,
								const vfield_t vz_u,
								const vfield_t vz_v,
								const vfield_t vz_w,
								const real_t  dt,
								const real_t  dy,
								const real_t  dx,
								const real_t  dz,
								const offset_t _SY,
								const offset_t _SX,
								const offset_t _SZ,
								const integer  y0,
								const integer  yf);


void stress_propagator( velocity_t   v,
						stress_t     s,
                       const real_t    dt,
                       const real_t    dy,
                       const real_t    dx,
                       const real_t    dz,
					   const integer   y0,
					   const integer   yf);

void source_insertion ( stress_t        v,
                        const real_t    dt,
                        const real_t    src, 
                        const real_t    dy,
                        const real_t    dx,
                        const real_t    dz);

#endif /* end of _FWI_PROPAGATOR_H_ definition */
