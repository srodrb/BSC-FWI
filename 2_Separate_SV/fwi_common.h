/*
 * =====================================================================================
 *
 *       Filename:  fwi_common.h
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  10/12/15 10:34:04
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Samuel Rodriguez Bernabeu, samuel.rodriguez@bsc.es
 *   Organization: 	Barcelona Supercomputing Center
 *
 * =====================================================================================
 */

#ifndef _FWI_COMMON_H_
#define _FWI_COMMON_H_

// When included before <stdlib.h>, solves implicit declaration of posix_memalign()
// http://stackoverflow.com/questions/32438554/warning-implicit-declaration-of-posix-memalign
#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>
#include <sys/time.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <omp.h>

#ifndef M_PI
	#define M_PI           3.14159265358979323846
#endif


/* data types definition */
typedef float  real_t;
typedef int    integer;

#define I "%d"     // integer printf symbol

typedef enum {RTM_KERNEL, FM_KERNEL         } propagator_t;
typedef enum {FORWARD   , BACKWARD, FWMODEL }  time_d;

/* simulation parameters */
extern const integer 	WRITTEN_FIELDS;
extern const integer 	HALO;
extern const integer 	SIMD_LENGTH;
extern const real_t    	IT_FACTOR;
extern const real_t    	IO_CHUNK_SIZE;

extern const size_t ALIGN_INT;
extern const size_t ALIGN_INTEGER;
extern const size_t ALIGN_REAL;

double TOGB(size_t bytes);

/*  Compiler compatiblity macros */
#ifdef __GNUC__
  /* http://stackoverflow.com/questions/25667901/assume-clause-in-gcc*/ \
    #define __assume(_cond) do { if (!(_cond)) __builtin_unreachable(); } while (0)
#endif


#define safe_fopen(filename, mode) 				fwi_fopen(filename, mode, __FILE__, __LINE__ )
#define safe_fclose(filename, stream) 			fwi_fclose(filename, stream, __FILE__, __LINE__ )
#define safe_fwrite(ptr, size, nmemb, stream) 	fwi_fwrite(ptr, size, nmemb, stream, __FILE__, __LINE__ )
#define safe_fread(ptr, size, nmemb, stream) 	fwi_fread(ptr, size, nmemb, stream, __FILE__, __LINE__ )

FILE* fwi_fopen  ( const char *filename, char *mode, char* srcfilename, int linenumber);
void  fwi_fclose ( const char *filename, FILE* stream, char* srcfilename, int linenumber);
void  fwi_fwrite ( void *ptr, size_t size, size_t nmemb, FILE *stream, char* srcfilename, int linenumber );
void  fwi_fread  ( void *ptr, size_t size, size_t nmemb, FILE *stream, char* srcfilename, int linenumber );

integer roundup(integer number, integer multiple);

int max_int( int a, int b);
int min_int( int a, int b);

double dtime(void);

void read_simulation_parameters (const char *fname,
                          real_t *lenz,
                          real_t *lenx,
                          real_t *leny,
                          real_t *vmin,
                          real_t *srclen,
                          real_t *rcvlen,
                          char *outputfolder);

void store_shot_parameters( int     shotid,
                           real_t    *vmin,
                           int     *stacki,
                           real_t    *dt,
                           int     *nt_fwd,
                           int     *nt_bwd,
                           real_t    *dz,
                           real_t    *dx,
                           real_t    *dy,
                           integer *dimmz,
                           integer *dimmx,
                           integer *dimmy,
                           char    *outputfolder);

void load_shot_parameters( int     shotid,
                          real_t    *vmin,
                          int     *stacki,
                          real_t    *dt,
                          int     *nt_fwd,
                          int     *nt_bwd,
                          real_t    *dz,
                          real_t    *dx,
                          real_t    *dy,
                          integer *dimmz,
                          integer *dimmx,
                          integer *dimmy,
                          char    *outputfolder);

void read_freqlist (  const char* filename,
					int *nfreqs,
					real_t **freqlist );

#define __malloc(alignment, size) fwi_malloc(alignment, size, __FILE__, __LINE__)
#define __free(ptr)               fwi_free(ptr, __FILE__, __LINE__)



void* fwi_malloc ( const size_t alignment, const integer size, const char* filename, int fileline);
void  fwi_free   ( void *ptr, const char* filename, int fileline );

int mkdir_p(const char *dir);

void create_folder(const char *folder);


#define print_error(M, ...)     fwi_writelog(__FILE__, __LINE__, __func__, "ERROR ", M, ##__VA_ARGS__)
#define print_info(M, ...)      fwi_writelog(__FILE__, __LINE__, __func__, "INFO  ", M, ##__VA_ARGS__)
#define print_stats(M, ...)     fwi_writelog(__FILE__, __LINE__, __func__, "STATS ", M, ##__VA_ARGS__)

#ifdef DEBUG
  #define print_debug(M, ...)  fwi_writelog(__FILE__, __LINE__, __func__, "DEBUG ", M, ##__VA_ARGS__)
#else
  #define print_debug(M, ...)
#endif

void fwi_writelog(const char *SourceFileName, 
                  const int LineNumber,
                  const char *FunctionName,
                  const char* MessageHeader,
                  const char *fmt,
                  ...);


#endif // end of _FWI_COMMON_H_ definition
