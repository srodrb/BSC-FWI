/*
 * =====================================================================================
 *
 *       Filename:  fwi_kernel.c
 *
 *    Description:  kernel propagator implementation
 *
 *        Version:  1.0
 *        Created:  14/12/15 12:10:05
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Samuel Rodriguez Bernabeu, samuel.rodriguez@bsc.es
 *   Organization:  Barcelona Supercomputing Center
 *
 * =====================================================================================
 */

#include "fwi_kernel.h"

/* field labels, just for creating nice paraview field names */
static const char *FIELD_NAMES[58] = {
	"tl_u","tl_v","tl_w",
	"tr_u","tr_v","tr_w",
	"bl_u","bl_v","bl_w",
	"br_u","br_v","br_w", 
	"tl_zz","tl_xz","tl_yz","tl_xx","tl_xy","tl_yy",
	"tr_zz","tr_xz","tr_yz","tr_xx","tr_xy","tr_yy",
	"bl_zz","bl_xz","bl_yz","bl_xx","bl_xy","bl_yy",
	"br_zz","br_xz","br_yz","br_xx","br_xy","br_yy",
	"C11","C12","C13","C14","C15","C16",
	"C22","C23","C24","C25","C26",
	"C33","C34","C35","C36",
	"C44","C45","C46",
	"C55","C56",
	"C66",
	"Rho"};


void set_v_field_to_random ( velocity_t volume, vfield_t idx )
{
	integer numberOfCells = (NZF * NXF * NYF);
	
	for(integer y=0; y < NYF; y++)
		for(integer x=0; x < NXF; x++)
			for(integer z=0; z < NZF; z++)
				volume[y][x][z][idx] = rand() / (1.0 * RAND_MAX);
};

void set_s_field_to_random ( stress_t volume, sfield_t idx )
{
	integer numberOfCells = (NZF * NXF * NYF);
	
	for(integer y=0; y < NYF; y++)
		for(integer x=0; x < NXF; x++)
			for(integer z=0; z < NZF; z++)
				volume[y][x][z][idx] = rand() / (1.0 * RAND_MAX);
};

void set_v_field_to_constant ( velocity_t volume, vfield_t idx, const real_t value)
{
	integer numberOfCells = (NZF * NXF * NYF);
	
	for(integer y=0; y < NYF; y++)
		for(integer x=0; x < NXF; x++)
			for(integer z=0; z < NZF; z++)
				volume[y][x][z][idx] = value;
};

void set_s_field_to_constant ( stress_t volume, sfield_t idx, const real_t value)
{
	integer numberOfCells = (NZF * NXF * NYF);
	
	for(integer y=0; y < NYF; y++)
		for(integer x=0; x < NXF; x++)
			for(integer z=0; z < NZF; z++)
				volume[y][x][z][idx] = value;
};


velocity_t alloc_v_volume(void)
{
	const size_t numberOfCells = (NYF * NXF * NZF);
    const size_t size = numberOfCells * sizeof(vcell_t);
    void* ptr = NULL;

    if ( (posix_memalign( &ptr, 4096, size)) != 0 ){
        print_error("Cant allocate memory for vector fields correctly");
        abort();
    }

	memset( ptr, 0, size);

    return ( (velocity_t) ptr );
};


stress_t alloc_s_volume(void)
{
	const size_t numberOfCells = (NYF * NXF * NZF);
    const size_t size = numberOfCells * sizeof(scell_t);
    void* ptr = NULL;

    if ( (posix_memalign( &ptr, 4096, size)) != 0 ){
        print_error("Cant allocate memory for vector fields correctly");
        abort();
    }

	memset( ptr, 0, size);

    return ( (stress_t) ptr );
};

void free_v_volume ( velocity_t volume )
{
    free( volume ); volume = NULL;
};

void free_s_volume ( stress_t volume )
{
    free( volume ); volume = NULL;
};

/*
void export_to_ParaView(volume_t volume,
		const real_t dy,
		const real_t dx,
		const real_t dz,
		const integer timestep)
{
#if defined(PARAVIEW_OUTPUT)
	char filename[100];
	sprintf( filename, "paraview_%.4d.vtk", timestep);
	FILE *fparaview = fopen( filename, "w");
	integer ncells = (integer) NYF * NXF * NXF;

	print_info("Exporting fields to ParaView VTK format");

	fprintf(fparaview, "# vtk DataFile Version 2.0\n");
	fprintf(fparaview, "FWI data set\n");
	fprintf(fparaview, "ASCII\n");
	fprintf(fparaview, "DATASET STRUCTURED_POINTS\n");
	fprintf(fparaview, "DIMENSIONS %d %d %d\n", (int) NYF, (int) NXF, (int) NZF);
	fprintf(fparaview, "SPACING %f %f %f\n", dy, dx, dz);
	fprintf(fparaview, "ORIGIN 0 0 0\n");
	fprintf(fparaview, "POINT_DATA %d\n", ncells);
	fprintf(fparaview, "FIELD FGS %d\n", N_CELL_FIELDS);

	for(integer idx=0; idx < N_CELL_FIELDS; idx++)
	{
		fprintf(fparaview, "%s 1 %d float\n", FIELD_NAMES[idx], ncells);
		
		for(integer y=0; y < NYF; y++) {
			for(integer x=0; x < NXF; x++) {
				for(integer z=0; z < NZF; z++) {
					fprintf(fparaview, "%f ", volume[y][x][z][idx]);
				}
			}
		}
		fprintf(fparaview, "\n");
	}

	fclose(fparaview);
	print_info("VTK file generated.");
#endif
};

void read_field( volume_t volume, field_t idx, FILE *stream)
{
	integer i=0;
	const integer numberOfCells = (NZF * NXF * NYF);
	real_t *tmp = (real_t*) __malloc( ALIGN_REAL, numberOfCells * sizeof(real_t));
	memset( tmp, 0, numberOfCells * sizeof(real_t));
    
	safe_fread( tmp, sizeof(real_t), numberOfCells, stream);

	for(integer y=0; y < NYF; y++)
		for(integer x=0; x < NXF; x++)
			for(integer z=0; z < NZF; z++)
				volume[y][x][z][idx] = tmp[i++];

	__free(tmp);
};

void write_field( volume_t volume, field_t idx, FILE *stream)
{
	integer i = 0;
	const integer numberOfCells = (NZF * NXF * NYF);
	real_t *tmp = (real_t*) __malloc( ALIGN_REAL, numberOfCells * sizeof(real_t));
	memset( tmp, 0, numberOfCells * sizeof(real_t));
	
	for(integer y=0; y < NYF; y++)
		for(integer x=0; x < NXF; x++)
			for(integer z=0; z < NZF; z++)
				tmp[i++] = volume[y][x][z][idx];

    safe_fwrite( tmp, sizeof(real_t), numberOfCells, stream);

	__free(tmp);
};


void print_field( volume_t volume, field_t idx , const char* message )
{
	if (( NZF > 20) || (NXF > 20) || (NYF > 20)) {
		print_info("Volume is too large, it does not make sense to print it!!");
		return;
	}
	
	if ( message ) print_info(message);
	
	integer z=0, x=0, y=0;
	
	for( integer y=0; y < NYF; y++){
		for( integer x=0; x < NXF; x++){
			for( integer z=0; z < NZF; z++ ) {
				fprintf(stderr, "%e  ", volume[y][x][z][idx] );
			}
			fprintf(stderr, "\n");
		}
		fprintf(stderr, "\n\n");
	}
}
*/

/*
 * Loads initial values from coeffs, stress and velocity.
 */
void load_initial_model  ( const real_t waveletFreq,
	   					velocity_t v,
						stress_t   s)
 {
	const size_t numberOfCells = (NZF * NXF * NYF);

    /* set volume to zero */
    memset( v, 0, numberOfCells * sizeof(vcell_t) );
    memset( s, 0, numberOfCells * sizeof(scell_t) );

#ifdef DO_NOT_PERFORM_IO /* initalize velocity components */
    print_error("Fields must be initizalized to random. NOT IMPLEMENTED YET!");

	set_s_field_to_random( s, C11);
	set_s_field_to_random( s, C12);
	set_s_field_to_random( s, C13);
	set_s_field_to_random( s, C14);
	set_s_field_to_random( s, C15);
	set_s_field_to_random( s, C16);
	set_s_field_to_random( s, C22);
	set_s_field_to_random( s, C23);
	set_s_field_to_random( s, C24);
	set_s_field_to_random( s, C25);
	set_s_field_to_random( s, C26);
	set_s_field_to_random( s, C33);
	set_s_field_to_random( s, C34);
	set_s_field_to_random( s, C35);
	set_s_field_to_random( s, C36);
	set_s_field_to_random( s, C44);
	set_s_field_to_random( s, C45);
	set_s_field_to_random( s, C46);
	set_s_field_to_random( s, C55);
	set_s_field_to_random( s, C56);
	set_s_field_to_random( s, C66);
	set_v_field_to_random( v, Rho);

#else /* load velocity model from external file */
    char modelname[300];
    real_t vp     = 0.0; // model velocity
    real_t rhocst = 1000.0;
	real_t vs     = 0.0;
	FILE *model   = NULL;
	
	/* Read model velocity from external file */
	sprintf( modelname, "../InputModels/velocitymodel_%.2f.bin", waveletFreq );
	print_info("Loading minimum velocity of the model from %s", modelname);
	model = safe_fopen( modelname, "rb" );
	safe_fread( &vp, sizeof(real_t), 1, model );
	safe_fclose ( "velocitymodel.bin", model );
	
	
	/* ----------------------------------------------------------
	 *       Initialize material coefficients
	 *       Calculate them as:
	 *           Cii = Vp² * rho (i=1:3)
	 *           Cii = Vs² * rho (i=4:6)
	 *           Cij = Vp²rho - 2Vs²rho- (ij = 12, 13 and 23) 
	 * ----------------------------------------------------------
	 */
	vp *= vp;
	vs *= vs;
	set_s_field_to_constant( s, C11, 1.0/(vp * rhocst));
	set_s_field_to_constant( s, C12, 1.0/((vp * rhocst) - 2.0*(vs * rhocst)));
	set_s_field_to_constant( s, C13, 1.0/((vp * rhocst) - 2.0*(vs * rhocst)));
	set_s_field_to_constant( s, C14, 1.0/(0.0));
	set_s_field_to_constant( s, C15, 1.0/(0.0));
	set_s_field_to_constant( s, C16, 1.0/(0.0));
	set_s_field_to_constant( s, C22, 1.0/(vp * rhocst));
	set_s_field_to_constant( s, C23, 1.0/((vp * rhocst) - 2.0*(vs * rhocst)));
	set_s_field_to_constant( s, C24, 1.0/(0.0));
	set_s_field_to_constant( s, C25, 1.0/(0.0));
	set_s_field_to_constant( s, C26, 1.0/(0.0));
	set_s_field_to_constant( s, C33, 1.0/(vp * rhocst));
	set_s_field_to_constant( s, C34, 1.0/(0.0));
	set_s_field_to_constant( s, C35, 1.0/(0.0));
	set_s_field_to_constant( s, C36, 1.0/(0.0));
	set_s_field_to_constant( s, C44, 1.0/(vs * rhocst));
	set_s_field_to_constant( s, C45, 1.0/(0.0));
	set_s_field_to_constant( s, C46, 1.0/(0.0));
	set_s_field_to_constant( s, C55, 1.0/(vs * rhocst));
	set_s_field_to_constant( s, C56, 1.0/(0.0));
	set_s_field_to_constant( s, C66, 1.0/(vs * rhocst));
	set_v_field_to_constant( v, Rho, rhocst );

#endif /* end of DDO_NOT_PERFOM_IO clause */
};


/*
 * Saves the complete velocity field to disk.
 */
void write_snapshot(char *folder,
                    int suffix,
					velocity_t v)
{
#ifdef DO_NOT_PERFORM_IO
	print_info("We are not writing the snapshot here cause IO is not enabled!");
#else

    char fname[300];
	FILE *fsnapshot = NULL;
    
    /* open snapshot file and write results */
    sprintf(fname,"%s/snapshot.%05d.bin", folder, suffix);
    print_debug("dumping snapshot %s\n", fname );
    fsnapshot = safe_fopen(fname,"wb");

	/* write the 9 velocity components at once */	
	for(integer y=0; y < NYF; y++)
		for(integer x=0; x < NXF; x++)
			for(integer z=0; z < NZF; z++)
				safe_fwrite( &(v[y][x][z][tl_u]), sizeof(real_t), 9, fsnapshot);

	safe_fclose(fname, fsnapshot );

#endif /* end of DO_NOT_PERFORM_IO clause */
};

/*
 * Reads the complete velocity field from disk.
 */
void read_snapshot(char *folder,
                   int suffix,
                   velocity_t v)
{
#ifdef DO_NOT_PERFORM_IO
    print_info("We are not reading the snapshot here cause IO is not enabled!");
#else
	char fname[300];
	FILE *fsnapshot = NULL;
    
    /* open snapshot file and write results */
    sprintf(fname,"%s/snapshot.%05d.bin", folder, suffix);
    print_debug("dumping snapshot %s\n", fname );
    fsnapshot = safe_fopen(fname,"rb");

	/* read the 9 velocity components at once */	
	for(integer y=0; y < NYF; y++)
		for(integer x=0; x < NXF; x++)
			for(integer z=0; z < NZF; z++)
				safe_fread( &(v[y][x][z][tl_u]), sizeof(real_t), 9, fsnapshot);

	safe_fclose(fname, fsnapshot );

#endif
};

void propagate    (time_d        direction,
					velocity_t   v,
					stress_t     s,
                    real_t        *wlv,
                    int           timesteps,
                    int           ntbwd,
                    real_t        dt,
                    real_t        dy,
                    real_t        dx,
                    real_t        dz,
                    integer       stacki,
                    char          *folder,
                    real_t        *dataflush )
{
	const integer BS = 2; /* blocking size */

    for(int t=0; t < timesteps; t++)
    {
        if( t % 10 == 0 ) print_info("Computing %d-th timestep", t);

		/* perform IO */
        if ( t%stacki == 0 && direction == FORWARD) write_snapshot(folder, t, v);

        /* perform IO */
        if ( t%stacki == 0 && direction == BACKWARD) read_snapshot(folder, ntbwd-t, v);

		for(integer y0=HALO; y0 < (NYF - HALO); y0+=BS) {
			/* compute integration limit */
			integer yf = min_int( NYF + BS, NYF - HALO);
        	
			/* ------------------------------------------------------------------------------ */
        	/*                      VELOCITY COMPUTATION                                      */
        	/* ------------------------------------------------------------------------------ */

			/* Phase 2. Computation of the central planes (maingrid). */
			velocity_propagator(v, s, dt, dy, dx, dy, y0, yf);
			
			/* ------------------------------------------------------------------------------ */
			/*                        STRESS COMPUTATION                                      */
			/* ------------------------------------------------------------------------------ */
	
			/* Phase 2 computation. Central planes of the domain (maingrid) */
			stress_propagator ( v, s, dt, dy, dx, dz, y0, yf);
		}		

		/* ------------------------------------------------------------------------------ */
		/*                        SOURCE INSERTION                                        */
		/* ------------------------------------------------------------------------------ */
		print_debug( "Source Insertion in timestep %d: Wlv value %f", t, wlv[t]);
		source_insertion( s, dt, wlv[t], dy, dx, dz);
//		export_to_ParaView( volume, dy, dx, dz, t );
    }
};
