#include "fwi_common.h"

/* extern variables declared in the header file */
const integer  WRITTEN_FIELDS =   12; /* >= 12.  */
const integer  HALO           =    4; /* >= 4    */ 
const integer  SIMD_LENGTH    =    8; /* # of real_t elements fitting into regs */
const real_t     IT_FACTOR      = 0.02;
const real_t     IO_CHUNK_SIZE  = 1024.f * 1024.f;

const size_t ALIGN_INT     = 16;
const size_t ALIGN_INTEGER = 16;
const size_t ALIGN_REAL    = 64;

int max_int( int a, int b)
{
    return ((a >= b) ? a : b);
};

int min_int( int a, int b)
{
    return ((a >= b) ? b : a);
};


inline double dtime(void)
{
	double tseconds = 0.0;
	struct timeval mytime;
	gettimeofday( &mytime, (struct timezone*) 0);
	tseconds = (double) (mytime.tv_sec + (double) mytime.tv_usec * 1.0e-6);
	return (tseconds);
};

inline double TOGB(size_t bytes)
{
  return (bytes / (1024.f * 1024.f * 1024.f));
};

/*
  This function is intended to round up a number (number) to the nearest multiple of the register
  size. In this way, we assure that the dimensions of the domain are suited to the most aggressive
  compiler optimizations.
 */
integer roundup(integer number, integer multiple)
{
    if (multiple == 0)
        return number;

    int remainder = number % multiple;
    if (remainder == 0)
        return number;

    return number + multiple - remainder;
};

/*
 NAME:create_folder
 PURPOSE:During execution creates temporal folders to organize necessary data for the execution

 folder      (in) name of the temporal folder created
 parent_rank (in) name of the rank related to the data archived in to the folder
 shotID      (in) identifier of the shot related to the data to be archived in to the folder

 RETURN none
 */
void create_folder(const char *folder)
{
    if (mkdir_p(folder) != 0) {
        print_error("cant create folder %s (Err code: %s)", folder, strerror(errno));
        exit(-1);
    }
    print_debug("Folder '%s' created",folder);
};

/*
 NAME: mkdir_p
 PURPOSE: creates the hierarchy of folders requested, if they do not exist.

 RETURN 0 if successful, !=0 otherwise
 */
int mkdir_p(const char *dir)
{
    char tmp[256];
    char *p = NULL;
    size_t len;

    snprintf(tmp, sizeof(tmp),"%s",dir);
    len = strlen(tmp);

    if(tmp[len - 1] == '/')
        tmp[len - 1] = 0;

    for(p = tmp + 1; *p; p++)
        if(*p == '/') {
            *p = 0;
            int rc = mkdir(tmp, S_IRWXU);
            if (rc != 0 && errno != EEXIST) {
                print_error("Error creating folder %s (Err code %s)", tmp, strerror(errno));
                return -1;
            }

            *p = '/';
        }

    int rc = mkdir(tmp, S_IRWXU);
    if (rc != 0 && errno != EEXIST) {
        print_error("Error creating folder %s (Err code %s)", tmp, strerror(errno));
        return -1;
    }

    return 0;
}

void read_simulation_parameters (const char *fname,
                          real_t *lenz,
                          real_t *lenx,
                          real_t *leny,
                          real_t *vmin,
                          real_t *srclen,
                          real_t *rcvlen,
                          char *outputfolder)
{
    FILE *fp = safe_fopen(fname, "r" );

    fscanf( fp, "%f\n", (real_t*) lenz   );
    fscanf( fp, "%f\n", (real_t*) lenx   );
    fscanf( fp, "%f\n", (real_t*) leny   );
    fscanf( fp, "%f\n", (real_t*) vmin   );
    fscanf( fp, "%f\n", (real_t*) srclen );
    fscanf( fp, "%f\n", (real_t*) rcvlen );

    /*
    * These three values are not needed for the shared memory implementation
    * they are here just to preserve compatilibility
    */
    int NotNeededValue;
    fscanf( fp, "%d\n", (int*) &NotNeededValue );
    fscanf( fp, "%d\n", (int*) &NotNeededValue );
    fscanf( fp, "%d\n", (int*) &NotNeededValue );
    fscanf( fp, "%d\n", (int*) &NotNeededValue );
    fscanf( fp, "%d\n", (int*) &NotNeededValue );

    /* Recover the value of the output directory path */
    fscanf( fp, "%s\n",  outputfolder  );

    print_debug("Len (z,x,y) (%f,%f,%f) vmin %f scrlen %f rcvlen %f outputfolder '%s'",
      *lenz, *lenx, *leny, *vmin, *srclen, *rcvlen, outputfolder );

    fclose(fp);
};


void store_shot_parameters( int     shotid,
                           real_t    *vmin,
                           int     *stacki,
                           real_t    *dt,
                           int    *nt_fwd,
                           int    *nt_bwd,
                           real_t    *dz,
                           real_t    *dx,
                           real_t    *dy,
                           integer *dimmz,
                           integer *dimmx,
                           integer *dimmy,
                           char    *outputfolder)
{
    char name[200];

    sprintf(name, "%s/shotparams_%05d.dat",outputfolder, shotid);

    print_debug("Storing parameters for shot %d into %s", shotid, name);

    FILE *fp = safe_fopen(name, "w");

    fprintf(fp, "%f\n",  (real_t   ) *vmin   );
    fprintf(fp, "%f\n",  (real_t   ) *dz     );
    fprintf(fp, "%f\n",  (real_t   ) *dx     );
    fprintf(fp, "%f\n",  (real_t   ) *dy     );
    fprintf(fp,  I"\n", (integer) *dimmz  );
    fprintf(fp,  I"\n", (integer) *dimmx  );
    fprintf(fp,  I"\n", (integer) *dimmy  );
    fprintf(fp, "%d\n",  (int    ) *nt_fwd );
    fprintf(fp, "%d\n",  (int    ) *nt_bwd );
    fprintf(fp, "%f\n",  (real_t   ) *dt     );
    fprintf(fp, "%d\n",  (int    ) *stacki );

    fclose(fp);
};

void load_shot_parameters( int    shotid,
                          real_t    *vmin,
                          int     *stacki,
                          real_t    *dt,
                          int     *nt_fwd,
                          int     *nt_bwd,
                          real_t    *dz,
                          real_t    *dx,
                          real_t    *dy,
                          integer *dimmz,
                          integer *dimmx,
                          integer *dimmy,
                          char    *outputfolder)
{
  FILE *fp = NULL;
    char name[200];

    sprintf(name, "%s/shotparams_%05d.dat",outputfolder, shotid);
    print_debug("Storing parameters for shot %d into %s", shotid, name);

    fp = safe_fopen(name, "r");

    fscanf(fp, "%f\n",  (real_t*   ) vmin   );
    fscanf(fp, "%f\n",  (real_t*   ) dz     );
    fscanf(fp, "%f\n",  (real_t*   ) dx     );
    fscanf(fp, "%f\n",  (real_t*   ) dy     );
    fscanf(fp,  I"\n", (integer*) dimmz  );
    fscanf(fp,  I"\n", (integer*) dimmx  );
    fscanf(fp,  I"\n", (integer*) dimmy  );
    fscanf(fp, "%d\n",  (int*    ) nt_fwd );
    fscanf(fp, "%d\n",  (int*    ) nt_bwd );
    fscanf(fp, "%f\n",  (real_t*   ) dt     );
    fscanf(fp, "%d\n",  (int*    ) stacki );

    safe_fclose( name, fp);
};

void read_freqlist( const char* filename, int *nfreqs, real_t **freqlist )
{
	int count  = 0;
	real_t freq;

	FILE *freqfile = safe_fopen( filename, "r");

	while( 1 )
	{
		int n = fscanf( freqfile, "%f", &freq);

		if ( n == 1 )
		{
			count += 1;
		}
	 	else if (errno != 0)
		{
			print_error("Error while reading freqlist file");
			break;
		}
		else if ( n == EOF )
		{
			break;
		}
	}


	/* Allocate memory for frequencies */
	*freqlist = (real_t*) __malloc( ALIGN_REAL, count * sizeof(real_t));

	/* return to initial position */
	fseek( freqfile, 0, SEEK_SET);
	count = 0;



	/* read again the file, storing the wavelet frequencies this time */
	while( 1 )
	{
		int n = fscanf( freqfile, "%f", &freq);

		if ( n == 1 )
		{
			(*freqlist)[count++] = freq;
		}
	 	else if (errno != 0)
		{
			print_error("Error while reading freqlist file");
			break;
		}
		else if ( n == EOF )
		{
			break;
		}
	}
	fclose( freqfile );

	*nfreqs = count;

	print_info("A total of %d frequencies were found...", *nfreqs );
	for( int i=0; i<count; i++)
		print_info("     %.3f Hz", (*freqlist)[i] );

};



void* fwi_malloc ( const size_t alignment, const integer size, const char* filename, int fileline)
{
  void *buffer;
  int error;

  if ( size <= 0 ) {
    print_error("Check for malloc overflow failed (size %d)!", size);
    abort();
  }

  if( (buffer = malloc( size )) == NULL ) {
    print_error("Cant allocate buffer correctly!");
    abort();
  }

/*
  if( (error=posix_memalign( &buffer, alignment, size)) != 0)
  {
    print_error("Cant allocate buffer correctly (called from %s, line %d)", filename, line);
    abort();
  }
*/
  return (buffer);
};


void  fwi_free   ( void *ptr, const char* filename, int fileline )
{
    free( ptr );
};

FILE* fwi_fopen(const char *filename, char *mode, char* srcfilename, int linenumber)
{
    FILE* temp = fopen( filename, mode);

    if( temp == NULL){
        print_error("Cant open filename %s, openmode '%s' (called from %s - %d)", filename, mode, srcfilename, linenumber);
        exit(-1);
    }
    return temp;
};

void fwi_fclose ( const char *filename, FILE* stream, char* srcfilename, int linenumber)
{
  if ( fclose( stream ) != 0)
  {
    print_error("Cant close filename %s (called from %s - %d)", filename, srcfilename, linenumber);
    abort();
  }

/*if ( unlink(filename)  != 0)
  {
    fprintf(stderr, "%s:%d: Cant unlink file %s correctly!\n", srcfilename, linenumber, filename );
    abort();
  }*/
};


void fwi_fwrite (void *ptr, size_t size, size_t nmemb, FILE *stream, char* srcfilename, int linenumber)
{
#ifdef DO_NOT_PERFORM_IO
  print_info("Warning: we are not doing any IO (called from %s).", __FUNCTION__);
#else
  if( stream == NULL ){
    print_error("Invalid stream\n");
    abort();
  }
  size_t res;

  double start = dtime();
	res = fwrite( ptr, size, nmemb, stream);
  double end = dtime() - start;

  double mbytes = (1.0 * size * nmemb) / (1024.0 * 1024.0);

  // fprintf(stderr, "Time %lf, elements %d bytes %d, MB %lf MB/s %lf\n", end, nmemb, size*nmemb, mbytes, mbytes / end);

	if( res != nmemb )
	{
		print_error("Error while fwrite (called from %s - %d)", srcfilename, linenumber );
		abort();
	}
#endif
};

void fwi_fread (void *ptr, size_t size, size_t nmemb, FILE *stream, char* srcfilename, int linenumber)
{
#ifdef DO_NOT_PERFORM_IO
    print_info("Warning: we are not doing any IO (called from %s).", __FUNCTION__);
#else
  if( stream == NULL ){
    print_error("Invalid\n");
    abort();
  }

	size_t res = fread( ptr, size, nmemb, stream);

	if( res != nmemb )
	{
		print_error("Cant fread (called from %s - %d)", srcfilename, linenumber);
    print_error("Trying to read %lu elements, only %lu were recovered", nmemb, res);
		abort();
	}
#endif
};



void fwi_writelog(const char *SourceFileName, 
                  const int LineNumber,
                  const char *FunctionName,
                  const char* MessageHeader,
                  const char *fmt,
                  ...)
{

//  const char LogFileName[] = "fwi.log";

  //FILE *fp = safe_fopen ( LogFileName, "a", __FILE__, __LINE__ );
  FILE *fp = stderr;
  
  va_list args;
  va_start(args, fmt);
  fprintf(fp, "%s:%s:%d:%s: ", MessageHeader, SourceFileName, LineNumber, FunctionName );
  vfprintf(fp, fmt, args);
  fprintf(fp, "\n");
  va_end(args);
  
  //safe_fclose ( LogFileName, fp, __FILE__, __LINE__);
};
