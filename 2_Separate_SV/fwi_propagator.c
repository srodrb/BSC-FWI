#include "fwi_propagator.h"

real_t v_stencil_Z (velocity_t volume,
				const offset_t   off,
				const vfield_t    idx,
                const real_t     dz,
                const integer    y,
                const integer    x,
                const integer    z)
{
  return  ((C0 * ( volume[y][x][z  +off][idx]  - volume[y][x][z-1+off][idx]) +
            C1 * ( volume[y][x][z+1+off][idx]  - volume[y][x][z-2+off][idx]) +
            C2 * ( volume[y][x][z+2+off][idx]  - volume[y][x][z-3+off][idx]) +
            C3 * ( volume[y][x][z+3+off][idx]  - volume[y][x][z-4+off][idx])) * dz );
};

real_t v_stencil_X(velocity_t volume,
				const offset_t off,
				const vfield_t idx,
                const real_t dx,
                const integer y,
                const integer x,
                const integer z)
{
	return ( (  C0 * ( volume[y][x  +off][z][idx]  - volume[y][x-1+off][z][idx]) +
                C1 * ( volume[y][x+1+off][z][idx]  - volume[y][x-2+off][z][idx]) +
                C2 * ( volume[y][x+2+off][z][idx]  - volume[y][x-3+off][z][idx]) +
                C3 * ( volume[y][x+3+off][z][idx]  - volume[y][x-4+off][z][idx])) * dx );
};

real_t v_stencil_Y(velocity_t volume,
				const offset_t off,
				const vfield_t idx,
                const real_t dy,
                const integer y,
                const integer x,
                const integer z)
{
  return ( (C0 * ( volume[y  +off][x][z][idx]  - volume[y-1+off][x][z][idx]) +
            C1 * ( volume[y+1+off][x][z][idx]  - volume[y-2+off][x][z][idx]) +
            C2 * ( volume[y+2+off][x][z][idx]  - volume[y-3+off][x][z][idx]) +
            C3 * ( volume[y+3+off][x][z][idx]  - volume[y-4+off][x][z][idx])) * dy );
};

real_t s_stencil_Z (stress_t volume,
				const offset_t   off,
				const sfield_t    idx,
                const real_t     dz,
                const integer    y,
                const integer    x,
                const integer    z)
{
  return  ((C0 * ( volume[y][x][z  +off][idx]  - volume[y][x][z-1+off][idx]) +
            C1 * ( volume[y][x][z+1+off][idx]  - volume[y][x][z-2+off][idx]) +
            C2 * ( volume[y][x][z+2+off][idx]  - volume[y][x][z-3+off][idx]) +
            C3 * ( volume[y][x][z+3+off][idx]  - volume[y][x][z-4+off][idx])) * dz );
};

real_t s_stencil_X(stress_t volume,
				const offset_t off,
				const sfield_t idx,
                const real_t dx,
                const integer y,
                const integer x,
                const integer z)
{
	return ( (  C0 * ( volume[y][x  +off][z][idx]  - volume[y][x-1+off][z][idx]) +
                C1 * ( volume[y][x+1+off][z][idx]  - volume[y][x-2+off][z][idx]) +
                C2 * ( volume[y][x+2+off][z][idx]  - volume[y][x-3+off][z][idx]) +
                C3 * ( volume[y][x+3+off][z][idx]  - volume[y][x-4+off][z][idx])) * dx );
};

real_t s_stencil_Y(stress_t volume,
				const offset_t off,
				const sfield_t idx,
                const real_t dy,
                const integer y,
                const integer x,
                const integer z)
{
  return ( (C0 * ( volume[y  +off][x][z][idx]  - volume[y-1+off][x][z][idx]) +
            C1 * ( volume[y+1+off][x][z][idx]  - volume[y-2+off][x][z][idx]) +
            C2 * ( volume[y+2+off][x][z][idx]  - volume[y-3+off][x][z][idx]) +
            C3 * ( volume[y+3+off][x][z][idx]  - volume[y-4+off][x][z][idx])) * dy );
};

/* -------------------------------------------------------------------- */
/*                     KERNELS FOR VELOCITY                             */
/* -------------------------------------------------------------------- */


real_t rho_BL (stress_t volume,
			const integer y,
            const integer x,
            const integer z)
{
    return (2.0f / (volume[y][x][z][Rho] + volume[y][x][z+1][Rho]));
};

real_t rho_TR (stress_t volume,
			const integer y,
            const integer x,
            const integer z)
{
    return (2.0f/ (volume[y][x][z][Rho] + volume[y][x+1][z][Rho]) );
};

real_t rho_BR (stress_t volume,
			const integer y,
            const integer x,
            const integer z)
{
    return ( 8.0f/ (volume[y  ][x  ][z  ][Rho] +
                    volume[y  ][x  ][z+1][Rho] +
                    volume[y  ][x+1][z  ][Rho] +
                    volume[y+1][x  ][z  ][Rho] +
                    volume[y+1][x+1][z  ][Rho] +
                    volume[y  ][x+1][z+1][Rho] +
                    volume[y+1][x  ][z+1][Rho] +
                    volume[y+1][x+1][z+1][Rho]) );
};

real_t rho_TL (stress_t volume,
			  const integer y,
              const integer x,
              const integer z)
{
    return (2.0f/ (volume[y][x][z][Rho] + volume[y+1][x][z][Rho]));
};

void compute_component_vcell_TL (velocity_t     v,
								stress_t        s,
								const vfield_t   VelField,
								const sfield_t   zStressField,
								const sfield_t   xStressField,
								const sfield_t   yStressField,
                                const real_t    dt,
                                const real_t    dy,
                                const real_t    dx,
                                const real_t    dz,
                                const offset_t _SY,
                                const offset_t _SX,
                                const offset_t _SZ,
								const integer  y0,
								const integer  yf)
{
    for(integer y=y0; y < yf; y++) {
        for(integer x=HALO; x < (NXF - HALO); x++) {
            for(integer z=HALO; z < (NZF - HALO); z++) {
                const real_t lrho = rho_TL    ( s, y, x, z);
                const real_t stx  = s_stencil_X ( s, _SX, xStressField, dx, y, x, z);
                const real_t sty  = s_stencil_Y ( s, _SY, yStressField, dy, y, x, z);
                const real_t stz  = s_stencil_Z ( s, _SZ, zStressField, dz, y, x, z);

				v[y][x][z][VelField] += ((stx + sty + stz) * dt * lrho);
            }
        }
    }
};

void compute_component_vcell_TR (velocity_t     v,
								stress_t        s,
								const vfield_t   VelField,
								const sfield_t   zStressField,
								const sfield_t   xStressField,
								const sfield_t   yStressField,
                                const real_t    dt,
                                const real_t    dy,
                                const real_t    dx,
                                const real_t    dz,
                                const offset_t _SY,
                                const offset_t _SX,
                                const offset_t _SZ,
								const integer  y0,
								const integer  yf)
{
    for(integer y=y0; y < yf; y++) {
        for(integer x=HALO; x < (NXF - HALO); x++) {
            for(integer z=HALO; z < (NZF - HALO); z++) {
                const real_t lrho = rho_TR    ( s, y, x, z);
                const real_t stx  = s_stencil_X ( s, _SX, xStressField, dx, y, x, z);
                const real_t sty  = s_stencil_Y ( s, _SY, yStressField, dy, y, x, z);
                const real_t stz  = s_stencil_Z ( s, _SZ, zStressField, dz, y, x, z);

				v[y][x][z][VelField] += ((stx + sty + stz) * dt * lrho);
            }
        }
    }
};

void compute_component_vcell_BL (velocity_t     v,
								stress_t        s,
								const vfield_t   VelField,
								const sfield_t   zStressField,
								const sfield_t   xStressField,
								const sfield_t   yStressField,
                                const real_t    dt,
                                const real_t    dy,
                                const real_t    dx,
                                const real_t    dz,
                                const offset_t _SY,
                                const offset_t _SX,
                                const offset_t _SZ,
								const integer  y0,
								const integer  yf)
{
    for(integer y=y0; y < yf; y++) {
        for(integer x=HALO; x < (NXF - HALO); x++) {
            for(integer z=HALO; z < (NZF - HALO); z++) {
                const real_t lrho = rho_BL    ( s, y, x, z);
                const real_t stx  = s_stencil_X ( s, _SX, xStressField, dx, y, x, z);
                const real_t sty  = s_stencil_Y ( s, _SY, yStressField, dy, y, x, z);
                const real_t stz  = s_stencil_Z ( s, _SZ, zStressField, dz, y, x, z);

				v[y][x][z][VelField] += ((stx + sty + stz) * dt * lrho);
            }
        }
    }
};



void compute_component_vcell_BR (velocity_t     v,
								stress_t        s,
								const vfield_t   VelField,
								const sfield_t   zStressField,
								const sfield_t   xStressField,
								const sfield_t   yStressField,
                                const real_t    dt,
                                const real_t    dy,
                                const real_t    dx,
                                const real_t    dz,
                                const offset_t _SY,
                                const offset_t _SX,
                                const offset_t _SZ,
								const integer  y0,
								const integer  yf)
{
    for(integer y=y0; y < yf; y++) {
        for(integer x=HALO; x < (NXF - HALO); x++) {
            for(integer z=HALO; z < (NZF - HALO); z++) {
                const real_t lrho = rho_BR    ( s, y, x, z);
                const real_t stx  = s_stencil_X ( s, _SX, xStressField, dx, y, x, z);
                const real_t sty  = s_stencil_Y ( s, _SY, yStressField, dy, y, x, z);
                const real_t stz  = s_stencil_Z ( s, _SZ, zStressField, dz, y, x, z);

				v[y][x][z][VelField] += ((stx + sty + stz) * dt * lrho);
            }
        }
    }
};

void velocity_propagator(velocity_t       v,
						stress_t          s,
                        const real_t      dt,
                        const real_t      dy,
                        const real_t      dx,
                        const real_t      dz,
						const integer     y0,
						const integer     yf)
{
	#pragma forceinline recursive
    {
		compute_component_vcell_TL (v, s, tl_w, bl_zz, tr_xz, tl_yz, dt, dy, dx, dz, back_offset, back_offset, forw_offset, y0, yf);
		compute_component_vcell_TR (v, s, tr_w, br_zz, tl_xz, tr_yz, dt, dy, dx, dz, back_offset, forw_offset, back_offset, y0, yf);
		compute_component_vcell_BL (v, s, bl_w, tl_zz, br_xz, bl_yz, dt, dy, dx, dz, forw_offset, back_offset, back_offset, y0, yf);
		compute_component_vcell_BR (v, s, br_w, tr_zz, bl_xz, br_yz, dt, dy, dx, dz, forw_offset, forw_offset, forw_offset, y0, yf);
		compute_component_vcell_TL (v, s, tl_u, bl_xz, tr_xx, tl_xy, dt, dy, dx, dz, back_offset, back_offset, forw_offset, y0, yf);
		compute_component_vcell_TR (v, s, tr_u, br_xz, tl_xx, tr_xy, dt, dy, dx, dz, back_offset, forw_offset, back_offset, y0, yf);
		compute_component_vcell_BL (v, s, bl_u, tl_xz, br_xx, bl_xy, dt, dy, dx, dz, forw_offset, back_offset, back_offset, y0, yf);
		compute_component_vcell_BR (v, s, br_u, tr_xz, bl_xx, br_xy, dt, dy, dx, dz, forw_offset, forw_offset, forw_offset, y0, yf);
		compute_component_vcell_TL (v, s, tl_v, bl_yz, tr_xy, tl_yy, dt, dy, dx, dz, back_offset, back_offset, forw_offset, y0, yf);
		compute_component_vcell_TR (v, s, tr_v, br_yz, tl_xy, tr_yy, dt, dy, dx, dz, back_offset, forw_offset, back_offset, y0, yf);
		compute_component_vcell_BL (v, s, bl_v, tl_yz, br_xy, bl_yy, dt, dy, dx, dz, forw_offset, back_offset, back_offset, y0, yf);
		compute_component_vcell_BR (v, s, br_v, tr_yz, bl_xy, br_yy, dt, dy, dx, dz, forw_offset, forw_offset, forw_offset, y0, yf);
    }
};


/* ------------------------------------------------------------------------------ */
/*                                                                                */
/*                               KERNELS FOR STRESS                               */
/*                                                                                */
/* ------------------------------------------------------------------------------ */



real_t cell_coeff_BR ( stress_t volume,
		const sfield_t idx,
		const integer y,
		const integer x,
		const integer z)
{
    return ( 1.0f /( 0.25f*(volume[y][x  ][z  ][idx] +
                            volume[y][x+1][z  ][idx] +
                            volume[y][x  ][z+1][idx] +
                            volume[y][x+1][z+1][idx])) );
};

real_t cell_coeff_TL (stress_t volume,
		const sfield_t idx,
		const integer y,
		const integer x,
		const integer z)
{
    return ( 1.0f / (volume[y][x][z][idx]));
};

real_t cell_coeff_BL (stress_t volume,
		const sfield_t idx,
		const integer y,
		const integer x,
		const integer z)
{
	return (1.0f/(0.25f*(volume[y  ][x][z  ][idx] +
                         volume[y+1][x][z  ][idx] +
                         volume[y  ][x][z+1][idx] +
                         volume[y+1][x][z+1][idx])) );
};

real_t cell_coeff_TR (stress_t volume,
		const sfield_t idx,
		const integer y,
		const integer x,
		const integer z)
{
    return (1.0f/ ( 0.25f *(volume[y  ][x  ][z][idx] +
                            volume[y  ][x+1][z][idx] +
                            volume[y+1][x  ][z][idx] +
                            volume[y+1][x+1][z][idx])));
};

real_t cell_coeff_ARTM_BR(stress_t volume,
		const sfield_t idx,
		const integer y,
		const integer x,
		const integer z)
{
    return ((1.0f / volume[y][x  ][z  ][idx]  +
             1.0f / volume[y][x+1][z  ][idx]  +
             1.0f / volume[y][x  ][z+1][idx]  +
             1.0f / volume[y][x+1][z+1][idx]) * 0.25f);
};

real_t cell_coeff_ARTM_TL(stress_t volume,
		const sfield_t idx,
		const integer y,
		const integer x,
		const integer z)
{
    return (1.0f / volume[y][x][z][idx]);
};

real_t cell_coeff_ARTM_BL(stress_t volume,
		const sfield_t idx,
		const integer y,
		const integer x,
		const integer z)
{
    return ((1.0f / volume[y  ][x][z  ][idx]  +
             1.0f / volume[y+1][x][z  ][idx]  +
             1.0f / volume[y  ][x][z+1][idx]  +
             1.0f / volume[y+1][x][z+1][idx]) * 0.25f);
};

real_t cell_coeff_ARTM_TR(stress_t volume,
		const sfield_t idx,
		const integer y,
		const integer x,
		const integer z)
{
    return ((1.0f / volume[y  ][x  ][z][idx]  +
             1.0f / volume[y  ][x+1][z][idx]  +
             1.0f / volume[y+1][x  ][z][idx]  +
             1.0f / volume[y+1][x+1][z][idx]) * 0.25f);
};

void stress_update (stress_t volume,
					const sfield_t stressField,
					const real_t c1,
					const real_t c2,
					const real_t c3,
					const real_t c4,
					const real_t c5,
					const real_t c6,
					const integer y,
					const integer x,
					const integer z,
					const real_t  dt,
					const real_t  u_x,
					const real_t  u_y,
					const real_t  u_z,
					const real_t  v_x,
					const real_t  v_y,
					const real_t  v_z,
					const real_t  w_x,
					const real_t  w_y,
					const real_t  w_z)
{
	volume[y][x][z][stressField] += dt * c1 * u_x;
	volume[y][x][z][stressField] += dt * c2 * v_y;
	volume[y][x][z][stressField] += dt * c3 * w_z;
	volume[y][x][z][stressField] += dt * c4 * (w_y + v_z);
	volume[y][x][z][stressField] += dt * c5 * (w_x + u_z);
	volume[y][x][z][stressField] += dt * c6 * (v_x + u_y);
};



void compute_component_scell_TR (velocity_t   v,
								stress_t      s,
								const vfield_t vx_u,
								const vfield_t vx_v,
								const vfield_t vx_w,
								const vfield_t vy_u,
								const vfield_t vy_v,
								const vfield_t vy_w,
								const vfield_t vz_u,
								const vfield_t vz_v,
								const vfield_t vz_w,
								const real_t  dt,
								const real_t  dy,
								const real_t  dx,
								const real_t  dz,
								const offset_t _SY,
								const offset_t _SX,
								const offset_t _SZ,
								const integer  y0,
								const integer  yf)
{
    for(integer y=y0; y < yf; y++) {
        for(integer x=HALO; x < (NXF - HALO); x++) {
            for(integer z=HALO; z < (NZF - HALO); z++) {
				const real_t c11 = cell_coeff_TR      (s, C11, y, x, z);
				const real_t c12 = cell_coeff_TR      (s, C12, y, x, z);
				const real_t c13 = cell_coeff_TR      (s, C13, y, x, z);
				const real_t c14 = cell_coeff_ARTM_TR (s, C14, y, x, z);
				const real_t c15 = cell_coeff_ARTM_TR (s, C15, y, x, z);
				const real_t c16 = cell_coeff_ARTM_TR (s, C16, y, x, z);
				const real_t c22 = cell_coeff_TR      (s, C22, y, x, z);
				const real_t c23 = cell_coeff_TR      (s, C23, y, x, z);
				const real_t c24 = cell_coeff_ARTM_TR (s, C24, y, x, z);
				const real_t c25 = cell_coeff_ARTM_TR (s, C25, y, x, z);
				const real_t c26 = cell_coeff_ARTM_TR (s, C26, y, x, z);
				const real_t c33 = cell_coeff_TR      (s, C33, y, x, z);
				const real_t c34 = cell_coeff_ARTM_TR (s, C34, y, x, z);
				const real_t c35 = cell_coeff_ARTM_TR (s, C35, y, x, z);
				const real_t c36 = cell_coeff_ARTM_TR (s, C36, y, x, z);
				const real_t c44 = cell_coeff_TR      (s, C44, y, x, z);
				const real_t c45 = cell_coeff_ARTM_TR (s, C45, y, x, z);
				const real_t c46 = cell_coeff_ARTM_TR (s, C46, y, x, z);
				const real_t c55 = cell_coeff_TR      (s, C55, y, x, z);
				const real_t c56 = cell_coeff_ARTM_TR (s, C56, y, x, z);
				const real_t c66 = cell_coeff_TR      (s, C66, y, x, z);

				const real_t u_x = v_stencil_X (v, _SX, vx_u, dx, y, x, z);
				const real_t v_x = v_stencil_X (v, _SX, vx_v, dx, y, x, z);
				const real_t w_x = v_stencil_X (v, _SX, vx_w, dx, y, x, z);

				const real_t u_y = v_stencil_Y (v, _SY, vy_u, dy, y, x, z);
				const real_t v_y = v_stencil_Y (v, _SY, vy_v, dy, y, x, z);
				const real_t w_y = v_stencil_Y (v, _SY, vy_w, dy, y, x, z);

				const real_t u_z = v_stencil_Z (v, _SZ, vz_u, dz, y, x, z);
				const real_t v_z = v_stencil_Z (v, _SZ, vz_v, dz, y, x, z);
				const real_t w_z = v_stencil_Z (v, _SZ, vz_w, dz, y, x, z);

				stress_update (s, tr_xx, c11, c12, c13, c14, c15, c16, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, tr_yy, c12, c22, c23, c24, c25, c26, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, tr_zz, c13, c23, c33, c34, c35, c36, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, tr_yz, c14, c24, c34, c44, c45, c46, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, tr_xz, c15, c25, c35, c45, c55, c56, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, tr_xy, c16, c26, c36, c46, c56, c66, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
			}
		}
	}
};

void compute_component_scell_TL (velocity_t   v,
								stress_t      s,
								const vfield_t vx_u,
								const vfield_t vx_v,
								const vfield_t vx_w,
								const vfield_t vy_u,
								const vfield_t vy_v,
								const vfield_t vy_w,
								const vfield_t vz_u,
								const vfield_t vz_v,
								const vfield_t vz_w,
								const real_t  dt,
								const real_t  dy,
								const real_t  dx,
								const real_t  dz,
								const offset_t _SY,
								const offset_t _SX,
								const offset_t _SZ,
								const integer  y0,
								const integer  yf)
{
    for(integer y=y0; y < yf; y++) {
        for(integer x=HALO; x < (NXF - HALO); x++) {
            for(integer z=HALO; z < (NZF - HALO); z++) {
				const real_t c11 = cell_coeff_TL      (s, C11, y, x, z);
				const real_t c12 = cell_coeff_TL      (s, C12, y, x, z);
				const real_t c13 = cell_coeff_TL      (s, C13, y, x, z);
				const real_t c14 = cell_coeff_ARTM_TL (s, C14, y, x, z);
				const real_t c15 = cell_coeff_ARTM_TL (s, C15, y, x, z);
				const real_t c16 = cell_coeff_ARTM_TL (s, C16, y, x, z);
				const real_t c22 = cell_coeff_TL      (s, C22, y, x, z);
				const real_t c23 = cell_coeff_TL      (s, C23, y, x, z);
				const real_t c24 = cell_coeff_ARTM_TL (s, C24, y, x, z);
				const real_t c25 = cell_coeff_ARTM_TL (s, C25, y, x, z);
				const real_t c26 = cell_coeff_ARTM_TL (s, C26, y, x, z);
				const real_t c33 = cell_coeff_TL      (s, C33, y, x, z);
				const real_t c34 = cell_coeff_ARTM_TL (s, C34, y, x, z);
				const real_t c35 = cell_coeff_ARTM_TL (s, C35, y, x, z);
				const real_t c36 = cell_coeff_ARTM_TL (s, C36, y, x, z);
				const real_t c44 = cell_coeff_TL      (s, C44, y, x, z);
				const real_t c45 = cell_coeff_ARTM_TL (s, C45, y, x, z);
				const real_t c46 = cell_coeff_ARTM_TL (s, C46, y, x, z);
				const real_t c55 = cell_coeff_TL      (s, C55, y, x, z);
				const real_t c56 = cell_coeff_ARTM_TL (s, C56, y, x, z);
				const real_t c66 = cell_coeff_TL      (s, C66, y, x, z);

				const real_t u_x = v_stencil_X (v, _SX, vx_u, dx, y, x, z);
				const real_t v_x = v_stencil_X (v, _SX, vx_v, dx, y, x, z);
				const real_t w_x = v_stencil_X (v, _SX, vx_w, dx, y, x, z);

				const real_t u_y = v_stencil_Y (v, _SY, vy_u, dy, y, x, z);
				const real_t v_y = v_stencil_Y (v, _SY, vy_v, dy, y, x, z);
				const real_t w_y = v_stencil_Y (v, _SY, vy_w, dy, y, x, z);

				const real_t u_z = v_stencil_Z (v, _SZ, vz_u, dz, y, x, z);
				const real_t v_z = v_stencil_Z (v, _SZ, vz_v, dz, y, x, z);
				const real_t w_z = v_stencil_Z (v, _SZ, vz_w, dz, y, x, z);
				
				stress_update (s, tr_xx, c11, c12, c13, c14, c15, c16, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, tr_yy, c12, c22, c23, c24, c25, c26, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, tr_zz, c13, c23, c33, c34, c35, c36, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, tr_yz, c14, c24, c34, c44, c45, c46, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, tr_xz, c15, c25, c35, c45, c55, c56, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, tr_xy, c16, c26, c36, c46, c56, c66, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
			}
		}
	}
};

void compute_component_scell_BR (velocity_t    v,
   								stress_t       s,
								const vfield_t vx_u,
								const vfield_t vx_v,
								const vfield_t vx_w,
								const vfield_t vy_u,
								const vfield_t vy_v,
								const vfield_t vy_w,
								const vfield_t vz_u,
								const vfield_t vz_v,
								const vfield_t vz_w,
								const real_t  dt,
								const real_t  dy,
								const real_t  dx,
								const real_t  dz,
								const offset_t _SY,
								const offset_t _SX,
								const offset_t _SZ,
								const integer  y0,
								const integer  yf)
{
    for(integer y=y0; y < yf; y++) {
        for(integer x=HALO; x < (NXF - HALO); x++) {
            for(integer z=HALO; z < (NZF - HALO); z++) {
				const real_t c11 = cell_coeff_BR      (s, C11, y, x, z);
				const real_t c12 = cell_coeff_BR      (s, C12, y, x, z);
				const real_t c13 = cell_coeff_BR      (s, C13, y, x, z);
				const real_t c22 = cell_coeff_BR      (s, C22, y, x, z);
				const real_t c23 = cell_coeff_BR      (s, C23, y, x, z);
				const real_t c33 = cell_coeff_BR      (s, C33, y, x, z);
				const real_t c44 = cell_coeff_BR      (s, C44, y, x, z);
				const real_t c55 = cell_coeff_BR      (s, C55, y, x, z);
				const real_t c66 = cell_coeff_BR      (s, C66, y, x, z);
				const real_t c14 = cell_coeff_ARTM_BR (s, C14, y, x, z);
				const real_t c15 = cell_coeff_ARTM_BR (s, C15, y, x, z);
				const real_t c16 = cell_coeff_ARTM_BR (s, C16, y, x, z);
				const real_t c24 = cell_coeff_ARTM_BR (s, C24, y, x, z);
				const real_t c25 = cell_coeff_ARTM_BR (s, C25, y, x, z);
				const real_t c26 = cell_coeff_ARTM_BR (s, C26, y, x, z);
				const real_t c34 = cell_coeff_ARTM_BR (s, C34, y, x, z);
				const real_t c35 = cell_coeff_ARTM_BR (s, C35, y, x, z);
				const real_t c36 = cell_coeff_ARTM_BR (s, C36, y, x, z);
				const real_t c45 = cell_coeff_ARTM_BR (s, C45, y, x, z);
				const real_t c46 = cell_coeff_ARTM_BR (s, C46, y, x, z);
				const real_t c56 = cell_coeff_ARTM_BR (s, C56, y, x, z);
		
				const real_t u_x = v_stencil_X (v, _SX, vx_u, dx, y, x, z);
				const real_t v_x = v_stencil_X (v, _SX, vx_v, dx, y, x, z);
				const real_t w_x = v_stencil_X (v, _SX, vx_w, dx, y, x, z);

				const real_t u_y = v_stencil_Y (v, _SY, vy_u, dy, y, x, z);
				const real_t v_y = v_stencil_Y (v, _SY, vy_v, dy, y, x, z);
				const real_t w_y = v_stencil_Y (v, _SY, vy_w, dy, y, x, z);
				
				const real_t u_z = v_stencil_Z (v, _SZ, vz_u, dz, y, x, z);
				const real_t v_z = v_stencil_Z (v, _SZ, vz_v, dz, y, x, z);
				const real_t w_z = v_stencil_Z (v, _SZ, vz_w, dz, y, x, z);

				stress_update (s, tr_xx, c11, c12, c13, c14, c15, c16, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, tr_yy, c12, c22, c23, c24, c25, c26, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, tr_zz, c13, c23, c33, c34, c35, c36, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, tr_yz, c14, c24, c34, c44, c45, c46, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, tr_xz, c15, c25, c35, c45, c55, c56, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, tr_xy, c16, c26, c36, c46, c56, c66, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
			}
		}
	}
};

void compute_component_scell_BL (velocity_t   v,
								stress_t      s,
								const vfield_t vx_u,
								const vfield_t vx_v,
								const vfield_t vx_w,
								const vfield_t vy_u,
								const vfield_t vy_v,
								const vfield_t vy_w,
								const vfield_t vz_u,
								const vfield_t vz_v,
								const vfield_t vz_w,
								const real_t  dt,
								const real_t  dy,
								const real_t  dx,
								const real_t  dz,
								const offset_t _SZ,
								const offset_t _SX,
								const offset_t _SY,
								const integer  y0,
								const integer  yf)
{
    for(integer y=y0; y < yf; y++) {
        for(integer x=HALO; x < (NXF - HALO); x++) {
            for(integer z=HALO; z < (NZF - HALO); z++) {
				const real_t c11 = cell_coeff_BL      (s, C11, y, x, z);
				const real_t c12 = cell_coeff_BL      (s, C12, y, x, z);
				const real_t c13 = cell_coeff_BL      (s, C13, y, x, z);
				const real_t c14 = cell_coeff_ARTM_BL (s, C14, y, x, z);
				const real_t c15 = cell_coeff_ARTM_BL (s, C15, y, x, z);
				const real_t c16 = cell_coeff_ARTM_BL (s, C16, y, x, z);
				const real_t c22 = cell_coeff_BL      (s, C22, y, x, z);
				const real_t c23 = cell_coeff_BL      (s, C23, y, x, z);
				const real_t c24 = cell_coeff_ARTM_BL (s, C24, y, x, z);
				const real_t c25 = cell_coeff_ARTM_BL (s, C25, y, x, z);
				const real_t c26 = cell_coeff_ARTM_BL (s, C26, y, x, z);
				const real_t c33 = cell_coeff_BL      (s, C33, y, x, z);
				const real_t c34 = cell_coeff_ARTM_BL (s, C34, y, x, z);
				const real_t c35 = cell_coeff_ARTM_BL (s, C35, y, x, z);
				const real_t c36 = cell_coeff_ARTM_BL (s, C36, y, x, z);
				const real_t c44 = cell_coeff_BL      (s, C44, y, x, z);
				const real_t c45 = cell_coeff_ARTM_BL (s, C45, y, x, z);
				const real_t c46 = cell_coeff_ARTM_BL (s, C46, y, x, z);
				const real_t c55 = cell_coeff_BL      (s, C55, y, x, z);
				const real_t c56 = cell_coeff_ARTM_BL (s, C56, y, x, z);
				const real_t c66 = cell_coeff_BL      (s, C66, y, x, z);

				const real_t u_x = v_stencil_X (v, _SX, vx_u, dx, y, x, z);
				const real_t v_x = v_stencil_X (v, _SX, vx_v, dx, y, x, z);
				const real_t w_x = v_stencil_X (v, _SX, vx_w, dx, y, x, z);
				
				const real_t u_y = v_stencil_Y (v, _SY, vy_u, dy, y, x, z);
				const real_t v_y = v_stencil_Y (v, _SY, vy_v, dy, y, x, z);
				const real_t w_y = v_stencil_Y (v, _SY, vy_w, dy, y, x, z);

				const real_t u_z = v_stencil_Z (v, _SZ, vz_u, dz, y, x, z);
				const real_t v_z = v_stencil_Z (v, _SZ, vz_v, dz, y, x, z);
				const real_t w_z = v_stencil_Z (v, _SZ, vz_w, dz, y, x, z);
				
				stress_update (s, bl_xx, c11, c12, c13, c14, c15, c16, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, bl_yy, c12, c22, c23, c24, c25, c26, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, bl_zz, c13, c23, c33, c34, c35, c36, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, bl_yz, c14, c24, c34, c44, c45, c46, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, bl_xz, c15, c25, c35, c45, c55, c56, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
				stress_update (s, bl_xy, c16, c26, c36, c46, c56, c66, y, x, z, dt, vx_u, vx_v, vx_w, vy_u, vy_v, vy_w, vz_u, vz_v, vz_w);
			}
		}
	}
};

void stress_propagator( velocity_t     v,
						stress_t       s,
                       const real_t    dt,
                       const real_t    dy,
                       const real_t    dx,
                       const real_t    dz,
					   const integer   y0,
					   const integer   yf)
{
	#pragma forceinline recursive
	{
		compute_component_scell_BR ( v, s, tr_u, tr_v, tr_w, bl_u, bl_v, bl_w, br_u, br_v, br_w, dt, dy, dx, dz, forw_offset, back_offset, forw_offset, y0, yf);
		compute_component_scell_BL ( v, s, tl_u, tl_v, tl_w, br_u, br_v, br_w, bl_u, bl_v, bl_w, dt, dy, dx, dz, forw_offset, forw_offset, back_offset, y0, yf);
		compute_component_scell_TR ( v, s, br_u, br_v, br_w, tl_u, tl_v, tl_w, tr_u, tr_v, tr_w, dt, dy, dx, dz, back_offset, forw_offset, forw_offset, y0, yf);
		compute_component_scell_TL ( v, s, bl_u, bl_v, bl_w, tr_u, tr_v, tr_w, tl_u, tl_v, tl_w, dt, dy, dx, dz, back_offset, back_offset, back_offset, y0, yf);
	}
};


void source_insertion ( stress_t         volume,
                        const real_t     dt,
                        const real_t     src, 
                        const real_t     dy,
                        const real_t     dx,
                        const real_t     dz)
{
    real_t src_value;
    integer z, x, y;
  
    /* Source insertion position */
    src_value = src * (dz * dx * ((dy>0)?dy:1));
    z = floorf((NZF-NZ0) / 2) + HALO;
    x = floorf((NXF-NX0) / 2) + HALO;
    y = floorf((NYF-NY0) / 2) + HALO;
    
    print_debug( "Source Insertion at mesh point (%d,%d,%d)\n", z, x, y );

    /* Source insertion */
	volume[y][x][z][tl_xx] += dt * src_value * EXP_TL;
	volume[y][x][z][tl_yy] += dt * src_value * EXP_TL;
	volume[y][x][z][tl_zz] += dt * src_value * EXP_TL;
	
	volume[y][x][z][br_xx] += dt * src_value * EXP_BR;
	volume[y][x][z][br_yy] += dt * src_value * EXP_BR;
	volume[y][x][z][br_zz] += dt * src_value * EXP_BR;

	/* Insertion at (y,x,z-1) */
	volume[y][x][z-1][br_xx] += dt * src_value * EXP_BR;
	volume[y][x][z-1][br_yy] += dt * src_value * EXP_BR;
	volume[y][x][z-1][br_zz] += dt * src_value * EXP_BR;

	/* Insertion at (y, x-1, z) */
	volume[y][x-1][z][br_xx] += dt * src_value * EXP_BR;
	volume[y][x-1][z][br_yy] += dt * src_value * EXP_BR;
	volume[y][x-1][z][br_zz] += dt * src_value * EXP_BR;

	/* Insertion at (y, x-1, z-1) */
   	volume[y][x-1][z][br_xx] += dt * src_value * EXP_BR;
	volume[y][x-1][z][br_yy] += dt * src_value * EXP_BR;
	volume[y][x-1][z][br_zz] += dt * src_value * EXP_BR;
   
	if( y > 0 ) {
		/* Insertion at (y,x,z) */
		volume[y][x][z][tr_xx] += dt * src_value * EXP_TR;
		volume[y][x][z][tr_yy] += dt * src_value * EXP_TR;
		volume[y][x][z][tr_zz] += dt * src_value * EXP_TR;
		
		/* Insertion at (y,x-1,z) */
		volume[y][x-1][z][tr_xx] += dt * src_value * EXP_TR;
		volume[y][x-1][z][tr_yy] += dt * src_value * EXP_TR;
		volume[y][x-1][z][tr_zz] += dt * src_value * EXP_TR;

		/* Insertion at (y-1,x,z) */
		volume[y-1][x][z][tr_xx] += dt * src_value * EXP_TR;
		volume[y-1][x][z][tr_yy] += dt * src_value * EXP_TR;
		volume[y-1][x][z][tr_zz] += dt * src_value * EXP_TR;

		/* Insertion at (y-1,x-1,z) */
		volume[y-1][x-1][z][tr_xx] += dt * src_value * EXP_TR;
		volume[y-1][x-1][z][tr_yy] += dt * src_value * EXP_TR;
		volume[y-1][x-1][z][tr_zz] += dt * src_value * EXP_TR;

		/* Insertion at (y,x,z) */
		volume[y][x][z][bl_xx] += dt * src_value * EXP_BL;
		volume[y][x][z][bl_yy] += dt * src_value * EXP_BL;
		volume[y][x][z][bl_zz] += dt * src_value * EXP_BL;
		
		/* Insertion at (y,x,z-1) */
		volume[y][x][z-1][bl_xx] += dt * src_value * EXP_BL;
		volume[y][x][z-1][bl_yy] += dt * src_value * EXP_BL;
		volume[y][x][z-1][bl_zz] += dt * src_value * EXP_BL;

		/* Insertion at (y-1,x,z) */
		volume[y-1][x][z][bl_xx] += dt * src_value * EXP_BL;
		volume[y-1][x][z][bl_yy] += dt * src_value * EXP_BL;
		volume[y-1][x][z][bl_zz] += dt * src_value * EXP_BL;

		/* Insertion at (y-1,x,z-1) */
		volume[y-1][x][z-1][bl_xx] += dt * src_value * EXP_BL;
		volume[y-1][x][z-1][bl_yy] += dt * src_value * EXP_BL;
		volume[y-1][x][z-1][bl_zz] += dt * src_value * EXP_BL;
	}
};
