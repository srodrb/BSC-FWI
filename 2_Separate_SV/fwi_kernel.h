/*
 * =====================================================================================
 *
 *       Filename:  fwi_kernel.h
 *
 *    Description:  Kernel propagator implementation
 *
 *        Version:  1.0
 *        Created:  14/12/15 12:10:24
 *       Revision:  none
 *       Compiler:  icc
 *
 *         Author:  Samuel Rodriguez Bernabeu, samuel.rodriguez@bsc.es
 *   Organization:  Barcelona Supercomputing Center
 *
 * =====================================================================================
 */

#ifndef _FWI_KERNEL_H_
#define _FWI_KERNEL_H_

#include "fwi_propagator.h"


void set_v_field_to_random (velocity_t volume, vfield_t idx);
void set_s_field_to_random (stress_t volume, sfield_t idx);

void set_v_field_to_constant(velocity_t volume, vfield_t idx, const real_t value);
void set_s_field_to_constant(stress_t volume, sfield_t idx, const real_t value);

velocity_t alloc_v_volume (void);
stress_t alloc_s_volume   (void);

void free_v_volume ( velocity_t volume );
void free_s_volume ( stress_t volume );

/* 
void export_to_ParaView(volume_t volume,
		const real_t dy,
		const real_t dx,
		const real_t dz,
		const integer timestep);
*/

/* --------------- I/O RELATED FUNCTIONS -------------------------------------- */

void load_initial_model ( const real_t waveletFreq,
		velocity_t v,
		stress_t s );

void write_snapshot (char         *folder,
                    const int     suffix,
                    velocity_t      volume );

void read_snapshot (char          *folder,
                    const int     suffix,
                    velocity_t     volume );

/* --------------- WAVE PROPAGATOR FUNCTIONS --------------------------------- */

void propagate (time_d        direction,
                velocity_t    v,
		  		stress_t      s,
                real_t        *wvl,
                int           timesteps,
                int           ntbwd,
                real_t        dt,
                real_t        dy,
                real_t        dx,
                real_t        dz,
                integer       stacki,
                char          *folder,
                real_t        *dataflush );

#endif /* end of _FWI_KERNEL_H_ definition */
